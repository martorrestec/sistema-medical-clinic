<?php
session_start();
require_once('../includes/server/urls.php');
if(!isset($_SESSION['auth'])) {
    header("Location: ".$GLOBALS['url_base']."/index.php?msg=Sessão%20Expirada!");
}
$GLOBALS['active-page'] = 'home';
?>
<!DOCTYPE html>
<html lang="pt-br">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="icon" type="image/png" sizes="16x16" href="<?= $GLOBALS['url_base'];?>/includes/image/favicon.png">
    
    <?php require_once('../includes/server/front/dependencias-css.php'); ?>

    <title>BEM VINDO AO SISTEMA MEDICAL CLINIC</title>
  </head>
  <body class="home-admin bg-admin">
  <div class="mt-3">
    <div class="container">
        <div class="row">
            <div class="col-md-3">
                <?php require_once('../includes/server/front/menu.php'); ?>
            </div>
            <div class="col-md-9 pt-sm-1 pt-md-5">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb bg-dark">
                        <li class="breadcrumb-item text-14"><a href="<?= $GLOBALS['url_base'];?>/admin/home.php">Sistema Medical Clinic</a></li>
                        <li class="breadcrumb-item text-14"><a href="<?= $GLOBALS['url_base'];?>/admin/home.php">Dashboard</a></li>
                        <li class="breadcrumb-item text-14 active" aria-current="page">Home</li>
                    </ol>
                </nav>
                <main>
                    <div class="row">
                        <div class="col-md-12 mb-4">
                            <div class="jumbotron text-center bg-light">
                                <h1 class="display-4">Erro 404</h1>
                                <p class="lead">A página solicitada não existe ou você não tem acesso!</p>
                                <hr class="my-4">
                                <a class="btn btn-success btn-lg" href="home.php" role="button">IR PARA HOME</a>
                            </div>
                        </div>
                    </div>
                    
                </main>
            </div>
        </div>
    </div>
  </div>
  <?php require_once('../includes/server/front/footer.php'); ?>
    <!-- Dependecias -->
    <?php require_once('../includes/server/front/dependencias-js.php'); ?>
  </body>
</html>
