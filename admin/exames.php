<?php
session_start();
require_once('../includes/server/urls.php');
if(!isset($_SESSION['auth'])) {
    header("Location: ".$GLOBALS['url_base']."/index.php?msg=Sessão%20Expirada!");
}
if($_SESSION['perfil'] == 'AT' || $_SESSION['perfil'] == 'ME') {
    header("Location: ".$GLOBALS['url_base']."/admin/404.php");
}
$GLOBALS['active-page'] = 'exames';
?>
<!DOCTYPE html>
<html lang="pt-br">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="icon" type="image/png" sizes="16x16" href="<?= $GLOBALS['url_base'];?>/includes/image/favicon.png">
    
    <?php require_once('../includes/server/front/dependencias-css.php'); ?>

    <title>EXAMES - BEM VINDO AO SISTEMA MEDICAL CLINIC</title>
  </head>
  <body class="home-admin bg-admin" id="page-exames">
  <div class="mt-3">
    <div class="container">
        <div class="row">
            <div class="col-md-3">
                <?php require_once('../includes/server/front/menu.php'); ?>
            </div>
            <div class="col-md-9 pt-sm-1 pt-md-5">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb bg-dark">
                        <li class="breadcrumb-item text-14"><a href="<?= $GLOBALS['url_base'];?>/admin/home.php">Sistema Medical Clinic</a></li>
                        <li class="breadcrumb-item text-14"><a href="<?= $GLOBALS['url_base'];?>/admin/home.php">Dashboard</a></li>
                        <li class="breadcrumb-item text-14 active" aria-current="page">Exames</li>
                    </ol>
                </nav>
                <main>
                    <div class="row">
                        <div class="col-md-12">
                            <h2 class="text-center mt-3 mb-4">Cadastro de Exames</h2>
                        </div>
                        <div class="col-md-4">
                            <button class="btn btn-sm btn-success modal-add"><i class="fas fa-file-medical mr-2"></i> Adicionar Exame</button>
                        </div>
                        <div class="col-md-8">
                            <form class="form-inline float-md-right float-left my-2 my-lg-0">
                                <input class="form-control mr-2 w-75 txtPesquisar" type="search" placeholder="Pesquisar Exame" aria-label="Pesquisar">
                                <button class="btn btn-outline-success my-2 my-sm-0 pesquisar" type="button"><i class="fas fa-search"></i></button>
                            </form>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12 mt-3">
                            <table class="table table-hover">
                                <thead>
                                    <tr class="table-success">
                                        <th class="text-center" width="10%" scope="col">Código</th>
                                        <th class="text-left" width="50%" scope="col">Nome do Exame</th>
                                        <th class="text-center" width="20%" scope="col">Sigla do Exame</th>
                                        <th class="text-center" width="20%" scope="col">Opções</th>
                                    </tr>
                                </thead>
                                <tbody id="linhaExame">
                                                                        
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12">
                            <nav aria-label="Navegação de página">
                                <ul class="pagination justify-content-center">
                                    <li class="page-item disabled">
                                        <a class="page-link text-14 text-white bg-success" href="#" tabindex="-1">Anterior</a>
                                    </li>
                                    <li class="page-item"><a class="page-link text-14 text-white bg-success" href="#">1</a></li>
                                    <li class="page-item"><a class="page-link text-14 text-white bg-success" href="#">2</a></li>
                                    <li class="page-item"><a class="page-link text-14 text-white bg-success" href="#">3</a></li>
                                    <li class="page-item">
                                        <a class="page-link text-14 text-white bg-success" href="#">Próximo</a>
                                    </li>
                                </ul>
                            </nav>
                        </div>
                    </div>
                                        
                </main>
            </div>
        </div>
    </div>
  </div>
  <?php require_once('../includes/server/front/footer.php'); ?>
  <?php require_once('../includes/server/front/modal-exames.php'); ?>

    <!-- Dependecias -->
    <?php require_once('../includes/server/front/dependencias-js.php'); ?>
    <script src="../includes/js/exames.js"></script>
  </body>
</html>
