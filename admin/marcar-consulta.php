<?php
session_start();
require_once('../includes/server/urls.php');
if(!isset($_SESSION['auth'])) {
    header("Location: ".$GLOBALS['url_base']."/index.php?msg=Sessão%20Expirada!");
}
$GLOBALS['active-page'] = 'consultas';
?>
<!DOCTYPE html>
<html lang="pt-br">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="icon" type="image/png" sizes="16x16" href="<?= $GLOBALS['url_base'];?>/includes/image/favicon.png">
    
    <?php require_once('../includes/server/front/dependencias-css.php'); ?>

    <title>CONSULTAS - BEM VINDO AO SISTEMA MEDICAL CLINIC</title>
  </head>
  <body class="home-admin bg-admin" id="page-consultas">
  <div class="mt-3">
    <div class="container">
        <div class="row">
            <div class="col-md-3">
                <?php require_once('../includes/server/front/menu.php'); ?>
            </div>
            <div class="col-md-9 pt-sm-1 pt-md-5">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb bg-dark">
                        <li class="breadcrumb-item text-14"><a href="<?= $GLOBALS['url_base'];?>/admin/home.php">Sistema Medical Clinic</a></li>
                        <li class="breadcrumb-item text-14"><a href="<?= $GLOBALS['url_base'];?>/admin/home.php">Dashboard</a></li>
                        <li class="breadcrumb-item text-14 active" aria-current="page">Marcar Consulta</li>
                    </ol>
                </nav>
                <main>
                    <div class="row">
                        <div class="col-md-12">
                            <h2 class="text-center mt-3 mb-4">Marcar Consulta</h2>
                        </div>
                        <div class="col-sm-12 col-md-4">
                            <button class="btn btn-sm btn-success d-none" data-toggle="modal" data-target=".add-usuario" > <i class="fas fa-user-plus mr-2"></i> Marcar Consulta</button>
                        </div>
                        <div class="col-sm-12 col-md-8">
                            <form class="form-inline float-md-right float-left my-2 my-lg-0">
                                <input class="form-control mr-2 w-75 d-none" type="search" placeholder="Pesquisar Usuário" aria-label="Pesquisar">
                                <a href="<?= $GLOBALS['url_base'];?>/admin/marcar-consulta.php" class="btn btn-outline-success my-2 my-sm-0 d-none" type="button"><i class="fas fa-search"></i></a>
                            </form>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12 mt-3">
                            <form action="" method="post">
                                <div class="form-row text-16">
                                    <div class="form-group col-md-2">
                                        <label for="codigo">Código</label>
                                        <input type="text" class="form-control" disabled name="txtCodigo" disabled id="codigo" required value="">
                                    </div>
                                    <div class="form-group col-md-10">
                                        <label for="nome">Nome</label>
                                        <input type="text" class="form-control" disabled name="txtNome" id="nome" required placeholder="">
                                    </div>
                                    <div class="form-group col-md-5">
                                        <label for="medico">Médico</label>
                                        <select name="txtMedico" class="form-control" id="medico">
                                            <option value="">Escolher...</option>
                                        </select>
                                    </div>
                                    <div class="form-group col-md-5">
                                        <label for="dataDisp">Data Disponível</label>
                                        <select name="txtDataDisp" disabled class="form-control" id="dataDisp">
                                            <option value="">Escolher...</option>                                            
                                        </select>
                                    </div> 
                                    <div class="form-group col-md-2 d-flex align-items-end justify-content-end">
                                        <button class="btn btn-sm btn-success form-control agendar">Agendar</button>
                                    </div>
                                                                                     
                                </div>
                                <div class="form-row text-16">
                                    
                                    <div class="form-group col-md-12 bg-light p-4 d-none painelConsulta">
                                        
                                        <h4 class="mb-3">Confirma dados da Consulta ?</h4>
                                        <p class="nomePaciente"></p>
                                        <p class="nomeMedico"></p>
                                        <p class="dataAtend"></p>
                                        <p class="horaAtend"></p>
                                        <button type="button" class="btn btn-sm btn-success marcarConsulta">Marcar</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    
                </main>
            </div>
        </div>
    </div>
  </div>
  <?php require_once('../includes/server/front/footer.php'); ?>
    <!-- Dependecias -->
    <?php require_once('../includes/server/front/dependencias-js.php'); ?>
    <script src="../includes/js/marcar-consultas.js"></script>
  </body>
</html>
