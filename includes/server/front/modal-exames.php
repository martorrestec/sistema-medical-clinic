<div class="modal fade modal-exames" tabindex="-1" role="dialog" aria-labelledby=".modal-exames" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <form>
        <div class="modal-header bg-green text-white">
            <h5 class="modal-title">Cadastrar Exame</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Fechar">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body text-16">
            <h4>Dados do Exame</h4>
            <div class="form-row">
              <div class="form-group col-md-2">
                <label for="codigo">Código</label>
                <input type="text" class="form-control" name="txtCodigo" disabled id="codigo" required value="">
              </div>
              <div class="form-group col-md-5">
                <label for="nomeExame">Nome do Exame</label>
                <input type="text" class="form-control" name="txtNomeExame" id="nomeExame" required placeholder="Ultrassonografia, Tórax">
              </div>
              <div class="form-group col-md-5">
              <label for="siglaExame">Sigla do Exame</label>
                <input type="text" class="form-control" name="txtSiglaExame" id="siglaExame" required placeholder="USTORAX">
              </div>                    
            </div>        
        </div><!-- fim .modal-body -->
        <div class="modal-footer">
            <button type="button" class="btn btn-dark" data-dismiss="modal">Cancelar</button>
            <button type="button" class="btn btn-success btn-action d-none incluirExame">Cadastrar</button>
            <button type="button" class="btn btn-success btn-action d-none consultarExame">Consultar</button>
            <button type="button" class="btn btn-success btn-action d-none alterarExame">Alterar</button>
        </div>
      </form>
    </div>
  </div><!-- fim do .modal-dialog -->
</div>

<div class="modal fade modal-excluir-dialog" tabindex="-1" role="dialog" aria-labelledby=".modal-excluir-dialog" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <form>
        <div class="modal-header bg-green text-white">
            <h5 class="modal-title">Excluir Exame</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Fechar">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body text-16">
            <p>Você confirma a exclusão do Exame <strong class="nomeExame">Ultrassonografia de Tórax (USTORAX)</strong> ?</p>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-dark" data-dismiss="modal">Cancelar</button>
            <button type="button" class="btn btn-danger excluirExame">Excluir</button>
        </div>
    </div>
      </form>
  </div>
</div>
