<div class="modal fade modal-usuario" tabindex="-1" role="dialog" aria-labelledby=".add-usuario" aria-hidden="true">
  <div class="modal-dialog modal-xl">
    <div class="modal-content">
      <form>
        <div class="modal-header bg-green text-white">
            <h5 class="modal-title">Cadastrar Usuário</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Fechar">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body text-16">
            <h4>Dados do Usuário</h4>
            <div class="form-row">
              <div class="form-group col-sm-2">
                <label for="codigo">Código</label>
                <input type="text" class="form-control" name="txtCodigo" disabled id="codigo" required value="">
              </div>
              <div class="form-group col-sm-10 col-md-10 col-xl-4">
                <label for="nome">Nome</label>
                <input type="text" class="form-control" name="txtNome" id="nome" required placeholder="Nome">
              </div>
              <div class="form-group  col-md-6 col-xl-3">
                <label for="email">Email</label>
                <input type="email" class="form-control" name="txtEmail" id="email" placeholder="Email">
              </div> 
              <div class="form-group col-md-6 col-xl-3">
                <label for="cargo">Cargo</label>
                <input type="cargo" class="form-control" name="txtCargo" id="cargo" placeholder="Cargo">
              </div>              
            </div>
            <div class="form-row">
              <div class="form-group col-sm-6 col-xl-3">
                <label for="login">Login</label>
                <input type="text" class="form-control" name="txtLogin" id="login" required placeholder="Nome para Login">
              </div>
              <div class="form-group col-sm-6 col-xl-3">
                <label for="senha">Senha</label>
                <input type="password" class="form-control" name="txtSenha" required id="senha" placeholder="Digite sua Senha">
              </div>
              <div class="form-group col-sm-6 col-xl-3">
                <label for="dtCad">Data de Cadastro</label>
                <input type="text" class="form-control" name="txtDtCad" id="dtCad" disabled placeholder="<?= date('d/m/Y') ?>">
              </div>   
              <div class="form-group col-sm-6 col-xl-3">
                <label for="perfil">Perfil</label>
                <select name="txtPerfil" class="form-control" id="perfil">
                  <option value="">Escolher...</option>
                  <option value="AT">Atendente</option>
                  <option value="ME">Médico</option>
                  <option value="GE">Gerente</option>
                  <option value="AD">Administrador</option>
                </select>
              </div> 
            </div>  
            <div class="form-row">
              <div class="form-group col-sm-12 col-xl-8 souMedico">
                <label class="d-block">Dias de Trabalho</label>
                <div class="form-check form-check-inline"> 
                  <label class="form-check-label mr-2">
                    <input class="form-check-input" type="checkbox" name="txtDiaTrabalho" id="diaTrabDom" value="dom"> Domingo
                  </label>           
                  <label class="form-check-label mr-2">
                    <input class="form-check-input" type="checkbox" name="txtDiaTrabalho" id="diaTrabSeg" value="seg"> Segunda
                  </label>
                  <label class="form-check-label mr-2">
                    <input class="form-check-input" type="checkbox" name="txtDiaTrabalho" id="diaTrabTer" value="ter"> Terça
                  </label>
                  <label class="form-check-label mr-2">
                    <input class="form-check-input" type="checkbox" name="txtDiaTrabalho" id="diaTrabQua" value="qua"> Quarta
                  </label>
                  <label class="form-check-label mr-2">
                    <input class="form-check-input" type="checkbox" name="txtDiaTrabalho" id="diaTrabQui" value="qui"> Quinta
                  </label>
                  <label class="form-check-label mr-2">
                    <input class="form-check-input" type="checkbox" name="txtDiaTrabalho" id="diaTrabSex" value="sex"> Sexta
                  </label>
                  <label class="form-check-label mr-2">
                    <input class="form-check-input" type="checkbox" name="txtDiaTrabalho" id="diaTrabSab" value="sab"> Sábado
                  </label>                  
                </div>
              </div>  
              <div class="form-group col-sm-12 col-xl-4 souMedico">
                <label class="d-block">Turno de Trabalho</label>
                <div class="form-check form-check-inline"> 
                  <label class="form-check-label mr-2">
                    <input class="form-check-input" type="checkbox" name="txtTurnoTrabalho" id="turnoTrabM" value="manha"> Manhã
                  </label>           
                  <label class="form-check-label mr-2">
                    <input class="form-check-input" type="checkbox" name="txtTurnoTrabalho" id="turnoTrabT" value="tarde"> Tarde
                  </label>
                  <label class="form-check-label mr-2">
                    <input class="form-check-input" type="checkbox" name="txtTurnoTrabalho" id="turnoTrabN" value="noite"> Noite
                  </label>                 
                </div>
              </div>           
            </div>          
        </div><!-- fim .modal-body -->
        <div class="modal-footer">
            <button type="button" class="btn btn-dark" data-dismiss="modal">Cancelar</button>
            <button type="button" class="btn btn-success btn-action d-none incluirUsuario">Cadastrar</button>
            <button type="button" class="btn btn-success btn-action d-none consultarUsuario">Consultar</button>
            <button type="button" class="btn btn-success btn-action d-none alterarUsuario">Alterar</button>
        </div>
      </form>
    </div>
  </div><!-- fim do .modal-dialog -->
</div>

<div class="modal fade modal-excluir-dialog" tabindex="-1" role="dialog" aria-labelledby=".modal-excluir-dialog" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <form>
        <div class="modal-header bg-green text-white">
            <h5 class="modal-title">Excluir Usuário</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Fechar">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body text-16">
            <p>Você confirma a exclusão do Usuário <strong class="nomeUsuario">Mark Arnold Jonson Obama</strong> ?</p>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-dark" data-dismiss="modal">Cancelar</button>
            <button type="button" class="btn btn-danger excluirUsuario">Excluir</button>
        </div>
      </form>
    </div>
  </div>
</div>

<div class="modal fade log-usuario" tabindex="-1" role="dialog" aria-labelledby=".log-usuario" aria-hidden="true">
  <div class="modal-dialog modal-xl">
    <div class="modal-content">
      <form>
        <div class="modal-header bg-green text-white">
            <h5 class="modal-title">Log de Acesso </h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Fechar">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
          <table class="table table-hover">
            <thead>
                <tr class="table-success">
                    <th class="text-center" width="10%" scope="col">Ação</th>
                    <th class="text-center" width="10%" scope="col">Código</th>
                    <th class="text-left" width="30%" scope="col">Nome Usuário</th>
                    <th class="text-center" width="17%" scope="col">Login</th>
                    <th class="text-center" width="33%" scope="col">Data e Hora</th>
                </tr>
            </thead>
            <tbody class="tabela-logs">
                  
                  <?php //include('logs/log_acesso_'.$_SESSION['id'].'.html'); ?>                              
            </tbody>
        </table>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-dark" data-dismiss="modal">Fechar</button>
        </div>
      </form>
    </div>
  </div>
</div>
