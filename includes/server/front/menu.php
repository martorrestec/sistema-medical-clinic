    
    <div class="text-center mb-2">
        <img src="<?= $GLOBALS['url_base'];?>/includes/image/logo_sistema_medical_clinic.png" class="img-fluid"/>
        <hr>
    </div>
    <div class="text-center mb-3">
        <a href="#" class="text-monospace d-block mb-3">
            <img src="<?= $GLOBALS['url_base'];?>/includes/image/avatar.png" />
        </a>
        <h5 class="text-14 text-capitalize">Usuário: <?= $_SESSION['nome']; ?></h5>
        <h6 class="text-14 text-capitalize">Perfil: <?= $_SESSION['descPerfil']; ?></h6>
        <h6 class="text-14">Data: <?= date('d/m/Y') ?></h6>
        <a class="btn btn-sm btn-dark" href="#">Editar Perfil</a>
        <a class="btn btn-sm btn-success" href="<?= $GLOBALS['url_base'];?>/logout.php">Sair</a>
    </div>
    <nav class="navbar navbar-expand-md navbar-dark">
        
        <button class="navbar-toggler bg-green w-100 p-3" type="button" data-toggle="collapse" data-target="#menu" aria-controls="menu" aria-expanded="false" aria-label="Alterna navegação">
            <span class="navbar-toggler-icon"></span>
        </button>
        
        <div class="collapse navbar-collapse" id="menu">
            <ul class="navbar-nav flex-column w-100">
                <li class="nav-item <?= ($GLOBALS['active-page']=='home') ? 'active' : ''; ?>"><a href="<?= $GLOBALS['url_base'];?>/admin/home.php" class="text-16 nav-link"><i class="fas fa-home mr-1"></i>Home</a></li>
                <li class="nav-item <?= ($GLOBALS['active-page']=='pacientes') ? 'active' : ''; ?>"><a href="<?= $GLOBALS['url_base'];?>/admin/pacientes.php" class="text-16 nav-link"><i class="fas fa-user mr-1"></i>Pacientes</a></li>
                <li class="nav-item <?= ($GLOBALS['active-page']=='consultas') ? 'active' : ''; ?>"><a href="<?= $GLOBALS['url_base'];?>/admin/consultas.php" class="text-16 nav-link"><i class="fas fa-book-medical mr-1"></i>Consultas</a></li>
                <?php 
                if($_SESSION['perfil'] == 'GE' || $_SESSION['perfil'] == 'AD') {
                    $activepage = ($GLOBALS["active-page"]=="medicamentos") ? "active" : "";
                    echo '<li class="nav-item '.$activepage.'"><a href="'.$GLOBALS["url_base"].'/admin/medicamentos.php" class="text-16 nav-link"><i class="fas fa-pills mr-1"></i>Medicamentos</a></li>';
                }  
                if($_SESSION['perfil'] == 'GE' || $_SESSION['perfil'] == 'AD') {
                    $activepage = ($GLOBALS["active-page"]=="exames") ? "active" : "";
                    echo '<li class="nav-item '.$activepage.'"><a href="'.$GLOBALS["url_base"].'/admin/exames.php" class="text-16 nav-link"><i class="fas fa-file-medical-alt mr-1"></i>Exames</a></li>';
                } 
                if($_SESSION['perfil'] == 'AT' || $_SESSION['perfil'] == 'GE' || $_SESSION['perfil'] == 'AD') {
                    $activepage = ($GLOBALS["active-page"]=="agenda") ? "active" : "";
                    echo '<li class="nav-item '.$activepage.'"><a href="'.$GLOBALS["url_base"].'/admin/agenda.php" class="text-16 nav-link"><i class="fas fa-calendar-alt mr-1"></i>Agenda</a></li>';
                }  
                if($_SESSION['perfil'] == 'AD') {
                    $activepage = ($GLOBALS["active-page"]=="usuarios") ? "active" : "";
                    echo '<li class="nav-item '.$activepage.'"><a href="'.$GLOBALS["url_base"].'/admin/usuarios.php" class="text-16 nav-link"><i class="fas fa-users-cog mr-1"></i>Usuários</a></li>';
                }               
                ?>
                
            </ul>
        </div>
    </nav>
    