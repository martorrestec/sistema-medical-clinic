<div class="modal fade modal-excluir-dialog" tabindex="-1" role="dialog" aria-labelledby=".modal-excluir-dialog" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <form>
        <div class="modal-header bg-green text-white">
            <h5 class="modal-title">Excluir Consulta</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Fechar">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body text-16">
            <p>Você confirma a exclusão da Consulta <strong class="nomePaciente"></strong> ?</p>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-dark" data-dismiss="modal">Cancelar</button>
            <button type="button" class="btn btn-danger excluirConsulta">Excluir</button>
        </div>
    </div>
      </form>
  </div>
</div>
