<div class="modal fade modal-pacientes" tabindex="-1" role="dialog" aria-labelledby=".modal-pacientes" aria-hidden="true">
  <div class="modal-dialog modal-xl">
    <div class="modal-content">
      <form>
        <div class="modal-header bg-green text-white">
            <h5 class="modal-title">Cadastrar Paciente</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Fechar">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body text-16">
            <h4>Dados Pessoais</h4>
            <div class="form-row">
              <div class="form-group col-md-2">
                <label for="codigo">Código</label>
                <input type="text" class="form-control" name="txtCodigo" disabled id="codigo" required value="">
              </div>
              <div class="form-group col-md-5">
                <label for="nome">Nome</label>
                <input type="text" class="form-control" name="txtNome" id="nome" required placeholder="Nome">
              </div>
              <div class="form-group col-md-5">
                <label for="email">Email</label>
                <input type="email" class="form-control" name="txtEmail" id="email" placeholder="Email">
              </div>              
            </div>
            <div class="form-row">
              <div class="form-group col-md-3">
                <label for="cpf">CPF</label>
                <input type="text" class="form-control" name="txtCpf" id="cpf" required placeholder="CPF">
              </div>
              <div class="form-group col-md-3">
                <label for="identidade">Identidade</label>
                <input type="text" class="form-control" name="txtIdent" id="identidade" placeholder="Identidade">
              </div>
              <div class="form-group col-md-2">
                <label for="dtNasc">Data de Nascimento</label>
                <input type="text" class="form-control" name="txtDtNasc" id="dtNasc" placeholder="01/01/1900">
              </div>   
              <div class="form-group col-md-2">
                <label for="estCivil">Estado Civil</label>
                <select name="txtEstCivil" class="form-control" id="estCivil">
                  <option value="">Escolher...</option>
                  <option value="casado">Casado</option>
                  <option value="solteiro">Solteiro</option>
                  <option value="viuvo">Viúvo</option>
                  <option value="divorciado">Divorciado</option>
                  <option value="outros">Outros</option>
                </select>
              </div>
              <div class="form-group col-md-2">
                <label for="sexo">Sexo</label>
                <select name="txtSexo" class="form-control" id="sexo">
                  <option value="">Escolher...</option>
                  <option value="F">Feminino</option>
                  <option value="M">Masculino</option>
                  <option value="N">Não Informar</option>
                  <option value="O">Outros</option>
                </select>
              </div>             
            </div>
            <div class="form-row">
              <div class="form-group col-md-2">
                <label for="phone">Telefone</label>
                <input type="text" class="form-control" name="txtPhone" id="phone" required placeholder="(21) 99999-9999">
              </div>
              <div class="form-group col-md-5">
                <label for="nomePai">Nome do Pai</label>
                <input type="text" class="form-control" name="txtNomePai" id="nomePai" required placeholder="Nome do Pai">
              </div>
              <div class="form-group col-md-5">
                <label for="nomeMae">Nome da Mãe</label>
                <input type="text" class="form-control" name="txtNomeMae" id="nomeMae" placeholder="Nome da Mãe">
              </div>              
            </div>
            <div class="form-row">
              <div class="form-group col-md-2">
                <label for="cep">CEP</label>
                <input type="text" class="form-control" name="txtCep" id="cep" placeholder="25555-555">
              </div>
              <div class="form-group col-md-8">
                <label for="logradouro">Endereço</label>
                <input type="text" class="form-control" name="txtLogradouro" id="logradouro" disabled placeholder="Rua dos Lobos">
              </div>
              <div class="form-group col-md-2">
                <label for="numero">Número</label>
                <input type="text" class="form-control" name="txtNumero" id="numero" placeholder="ex.: 1024">
              </div>
            </div>
            <div class="form-row">
              <div class="form-group col-md-4">
                <label for="complemento">Complemento</label>
                <input type="text" class="form-control" name="txtComplemento" id="complemento" placeholder="ex.: apto.: 103">
              </div>
              <div class="form-group col-md-3">
                <label for="bairro">Bairro</label>
                <input type="text" class="form-control" disabled name="txtBairro" id="bairro">
              </div>      
              <div class="form-group col-md-3">
                <label for="cidade">Cidade</label>
                <input type="text" class="form-control" disabled name="txtCidade" id="cidade">
              </div>
              <div class="form-group col-md-2">
                <label for="estado">Estado</label>
                <select name="txtEstado" id="estado" disabled class="form-control">
                  <option >Escolher...</option>
                  <option value="AC">Acre</option>
                  <option value="AL">Alagoas</option>
                  <option value="AP">Amapá</option>
                  <option value="AM">Amazonas</option>
                  <option value="BA">Bahia</option>
                  <option value="CE">Ceará</option>
                  <option value="DF">Distrito Federal</option>
                  <option value="ES">Espírito Santo</option>
                  <option value="GO">Goiás</option>
                  <option value="MA">Maranhão</option>
                  <option value="MT">Mato Grosso</option>
                  <option value="MS">Mato Grosso do Sul</option>
                  <option value="MG">Minas Gerais</option>
                  <option value="PA">Pará</option>
                  <option value="PB">Paraíba</option>
                  <option value="PR">Paraná</option>
                  <option value="PE">Pernambuco</option>
                  <option value="PI">Piauí</option>
                  <option value="RJ">Rio de Janeiro</option>
                  <option value="RN">Rio Grande do Norte</option>
                  <option value="RS">Rio Grande do Sul</option>
                  <option value="RO">Rondônia</option>
                  <option value="RR">Roraima</option>
                  <option value="SC">Santa Catarina</option>
                  <option value="SP">São Paulo</option>
                  <option value="SE">Sergipe</option>
                  <option value="TO">Tocantins</option>
                  <option value="EX">Estrangeiro</option>
                </select>
              </div>              
            </div>          
        </div><!-- fim .modal-body -->
        <div class="modal-footer">
            <button type="button" class="btn btn-dark" data-dismiss="modal">Cancelar</button>
            <button type="button" class="btn btn-success btn-action d-none incluirPaciente">Cadastrar</button>
            <button type="button" class="btn btn-success btn-action d-none consultarPaciente">Consultar</button>
            <button type="button" class="btn btn-success btn-action d-none alterarPaciente">Alterar</button>
        </div>
      </form>
    </div>
  </div><!-- fim do .modal-dialog -->
</div>


<div class="modal fade modal-excluir-dialog" tabindex="-1" role="dialog" aria-labelledby=".modal-excluir-dialog" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <form>
        <div class="modal-header bg-green text-white">
            <h5 class="modal-title">Excluir Paciente</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Fechar">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body text-16">
            <p>Você confirma a exclusão do Paciente <strong  class="nomePaciente">Mark Arnold Jonson Obama</strong> ?</p>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-dark" data-dismiss="modal">Cancelar</button>
            <button type="button" class="btn btn-danger excluirPaciente">Excluir</button>
        </div>
    </div>
      </form>
  </div>
</div>
<div class="modal fade modal-historico" tabindex="-1" role="dialog" aria-labelledby=".modal-historico" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <form>
        <div class="modal-header bg-green text-white">
            <h5 class="modal-title">Histórico de Consulta</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Fechar">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body text-16">
            <div class="row">
                <div class="col-12 mt-3">
                    <table class="table table-hover" style="font-size:10px;">
                        <thead>
                            <tr class="table-success">
                                <th class="text-center" width="8%" scope="col">Código</th>
                                <th class="text-left" width="30%" scope="col">Médico</th>
                                <th class="text-center" width="15%" scope="col">Data</th>
                                <th class="text-center" width="10%" scope="col">Hora</th>
                                <th class="text-left" width="30%" scope="col">Nome Paciente</th>
                            </tr>
                        </thead>
                        <tbody id="linhaConsulta">
                                                                
                        </tbody>
                    </table>
                </div>
            </div>       
        </div><!-- fim .modal-body -->
        <div class="modal-footer">
            <button type="button" class="btn btn-dark" data-dismiss="modal">Cancelar</button>
        </div>
      </form>
    </div>
  </div><!-- fim do .modal-dialog -->
</div>

