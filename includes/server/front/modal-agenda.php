<div class="modal fade modal-add" tabindex="-1" role="dialog" aria-labelledby=".modal-add" aria-hidden="true">
  <div class="modal-dialog modal-xl">
    <div class="modal-content">
      <form>
        <div class="modal-header bg-green text-white">
            <h5 class="modal-title">Gerenciamento da Agenda</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Fechar">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body text-16">
            <div class="row">
                <div class="col-sm-12">
                    <p><strong>Médico: Dr. </strong><span class="nomeMedico"></span></p>
                    <p><strong>Dias de Atendimento: </strong><span class="diasAtend text-uppercase"></span></p>
                    <p><strong>Turnos de Atendimento: </strong><span class="turnoAtend  text-uppercase"></span></p>
                </div>
            </div>
            <div class="form-row">
              <div class="form-inline col-md-12">
                <label for="dataAgend">Abrir Agendamento:</label>
                <input type="date" class="form-control mx-3" name="txtDataAgend" id="dataAgend">
                <input type="time" class="form-control mr-3" name="txtHoraAgend" id="horaAgend">
                <button type="button" class="btn btn-success incluirAgenda">Adicionar Data</button>
              </div>                            
            </div> 
            <div class="row">
                <div class="col-12 mt-3">
                    <hr>
                    <table class="table table-hover" style="font-size:10px;">
                        <thead>
                            <tr class="table-success">
                                <th class="text-center" width="8%" scope="col">Código</th>
                                <th class="text-left" width="30%" scope="col">Médico</th>
                                <th class="text-center" width="15%" scope="col">Data</th>
                                <th class="text-center" width="10%" scope="col">Hora</th>
                                <th class="text-left" width="30%" scope="col">Nome Paciente</th>
                                <th class="text-center" width="7%" scope="col">Opções</th>
                            </tr>
                        </thead>
                        <tbody id="linhaConsulta">
                                                                
                        </tbody>
                    </table>
                </div>
            </div>       
        </div><!-- fim .modal-body -->
        <div class="modal-footer">
            <button type="button" class="btn btn-dark" data-dismiss="modal">Cancelar</button>
        </div>
      </form>
    </div>
  </div><!-- fim do .modal-dialog -->
</div>

