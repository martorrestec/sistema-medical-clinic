<div class="modal fade modal-medicamentos" tabindex="-1" role="dialog" aria-labelledby=".modal-medicamentos" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <form>
        <div class="modal-header bg-green text-white">
            <h5 class="modal-title">Cadastrar Medicamento</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Fechar">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body text-16">
            <h4>Dados do Medicamento</h4>
            <div class="form-row">
              <div class="form-group col-md-2">
                <label for="codigo">Código</label>
                <input type="text" class="form-control" name="txtCodigo" disabled id="codigo" required value="">
              </div>
              <div class="form-group col-md-5">
                <label for="nomeGenerico">Nome Genérico</label>
                <input type="text" class="form-control" name="txtNomeGenerico" id="nomeGenerico" required placeholder="Acetilcisteína">
              </div>
              <div class="form-group col-md-5">
              <label for="nomeFabrica">Nome de Fábrica</label>
                <input type="text" class="form-control" name="txtNomeFabrica" id="nomeFabrica" required placeholder="Fluimicil">
              </div>              
            </div>
            <div class="form-row">
              <div class="form-group col-md-4">
                <label for="formaFarma">Forma Farmacêutica</label>
                <input type="text" class="form-control" name="txtFormaFarma" id="formaFarma" required placeholder="Xarope">
              </div>
              <div class="form-group col-md-4">
                <label for="fabricante">Fabricante</label>
                <input type="text" class="form-control" name="txtFabricante" id="fabricante" placeholder="Europharma">
              </div>
              <div class="form-group col-md-4">
                <label for="concentracao">Concentração</label>
                <input type="text" class="form-control" name="txtConcentracao" id="concentracao" placeholder="100mg/ml">
              </div>       
            </div>        
        </div><!-- fim .modal-body -->
        <div class="modal-footer">
            <button type="button" class="btn btn-dark" data-dismiss="modal">Cancelar</button>
            <button type="button" class="btn btn-success btn-action d-none incluirMedicamento">Cadastrar</button>
            <button type="button" class="btn btn-success btn-action d-none consultarMedicamento">Consultar</button>
            <button type="button" class="btn btn-success btn-action d-none alterarMedicamento">Alterar</button>
        </div>
      </form>
    </div>
  </div><!-- fim do .modal-dialog -->
</div>
<!--
<div class="modal fade alterar-medicamento" tabindex="-1" role="dialog" aria-labelledby=".alterar-medicamento" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <form>
        <div class="modal-header bg-green text-white">
            <h5 class="modal-title">Alterar Medicamento</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Fechar">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body text-16">
            <h4>Dados do Medicamento</h4>
            <div class="form-row">
              <div class="form-group col-md-2">
                <label for="codigo">Código</label>
                <input type="text" class="form-control" name="txtCodigo" disabled id="codigo" required value="4001">
              </div>
              <div class="form-group col-md-5">
                <label for="nomeGenerico">Nome Genérico</label>
                <input type="text" class="form-control" name="txtNomeGenerico" id="nomeGenerico" required placeholder="Acetilcisteína">
              </div>
              <div class="form-group col-md-5">
              <label for="nomeFabrica">Nome de Fábrica</label>
                <input type="text" class="form-control" name="txtNomeFabrica" id="nomeFabrica" required placeholder="Fluimicil">
              </div>              
            </div>
            <div class="form-row">
              <div class="form-group col-md-4">
                <label for="formaFarma">Forma Farmacêutica</label>
                <input type="text" class="form-control" name="txtFormaFarma" id="formaFarma" required placeholder="Xarope">
              </div>
              <div class="form-group col-md-4">
                <label for="fabricante">Fabricante</label>
                <input type="text" class="form-control" name="txtFabricante" id="fabricante" placeholder="Europharma">
              </div>
              <div class="form-group col-md-4">
                <label for="concentracao">Concentração</label>
                <input type="text" class="form-control" name="txtConcentracao" id="concentracao" placeholder="100mg/ml">
              </div>       
            </div> 
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-dark" data-dismiss="modal">Cancelar</button>
            <button type="button" class="btn btn-success">Salvar</button>
        </div>
      </form>
      </div>
  </div>
</div>

<div class="modal fade consultar-medicamento" tabindex="-1" role="dialog" aria-labelledby=".alterar-medicamento" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <form>
        <div class="modal-header bg-green text-white">
            <h5 class="modal-title">Consultar Medicamento</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Fechar">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body text-16">
            <h4>Dados do Medicamento</h4>
            <div class="form-row">
              <div class="form-group col-md-2">
                <label for="codigo">Código</label>
                <input type="text" class="form-control" name="txtCodigo" disabled id="codigo" required value="4001">
              </div>
              <div class="form-group col-md-5">
                <label for="nomeGenerico">Nome Genérico</label>
                <input type="text" class="form-control" name="txtNomeGenerico" disabled id="nomeGenerico" required placeholder="Acetilcisteína">
              </div>
              <div class="form-group col-md-5">
              <label for="nomeFabrica">Nome de Fábrica</label>
                <input type="text" class="form-control" name="txtNomeFabrica" disabled id="nomeFabrica" required placeholder="Fluimicil">
              </div>              
            </div>
            <div class="form-row">
              <div class="form-group col-md-4">
                <label for="formaFarma">Forma Farmacêutica</label>
                <input type="text" class="form-control" name="txtFormaFarma" disabled id="formaFarma" required placeholder="Xarope">
              </div>
              <div class="form-group col-md-4">
                <label for="fabricante">Fabricante</label>
                <input type="text" class="form-control" name="txtFabricante" disabled id="fabricante" placeholder="Europharma">
              </div>
              <div class="form-group col-md-4">
                <label for="concentracao">Concentração</label>
                <input type="text" class="form-control" name="txtConcentracao" disabled id="concentracao" placeholder="100mg/ml">
              </div>       
            </div>           
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-dark" data-dismiss="modal">Fechar</button>
        </div>
      </form>      
    </div>
  </div>
</div>
-->
<div class="modal fade modal-excluir-dialog" tabindex="-1" role="dialog" aria-labelledby=".modal-excluir-dialog" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <form>
        <div class="modal-header bg-green text-white">
            <h5 class="modal-title">Excluir Medicamento</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Fechar">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body text-16">
        <p>Você confirma a exclusão do Medicamento <strong class="nomeMedicamento">Fluimicil</strong> ?</p>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-dark" data-dismiss="modal">Cancelar</button>
            <button type="button" class="btn btn-danger excluirMedicamento">Excluir</button>
        </div>
    </div>
      </form>
  </div>
</div>
