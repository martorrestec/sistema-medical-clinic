<?php

function RecuperaForm() {
	
	$post = $_REQUEST;

	// Verifica se todos campos vieram pelo formulário e se estão preenchidos
	if(isset($post['filtro']) || isset($post['valor']) || isset($post['modulo'])){
		foreach ($post as $key => $value) {
			$GLOBALS[$key] = filter_var($value);
        }//fim do foreach
        
	}//fim do if
    
}//fim do RecuperaForm

function ConsultaDados() {
	require_once('../config.php');
	
	// Prepara uma sentença para ser executada
	$sql = "SELECT * FROM agendaMedica WHERE id_paciente != ''";

	$statement = $pdo->prepare($sql);

	// Executa a sentença já com os valores
	if($statement->execute()){	    
		$resultado = $statement->fetchAll(PDO::FETCH_ASSOC);
        echo json_encode($resultado, JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);
        //print_r($resultado);  
		//echo $sql;
	} else {
	    // Definimos a mensagem de erro
	    echo 'Sistema Fora do Ar! Tente mais tarde...';; 
	} 

}//fim do ConsultaDados

function ConsultaEspecifica() {
	require_once('../config.php');

	// Prepara uma sentença para ser executada
	$sql = "SELECT * FROM paciente WHERE id = {$GLOBALS['id']}";

	$statement = $pdo->prepare($sql);

	// Executa a sentença já com os valores
	if($statement->execute()){	    
		$resultado = $statement->fetchAll(PDO::FETCH_ASSOC);
		 
		echo json_encode($resultado, JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);
		//print_r($resultado);  
		//echo $resultado;
	} else {
	    // Definimos a mensagem de erro
	    echo 'Sistema Fora do Ar! Tente mais tarde...';; 
	} 

}//fim do ConsultaEspecifica

function Incluir() {

	require_once('../config.php');
	
    // Prepara uma sentença para ser executada    
    $sql = "UPDATE agendaMedica SET id_paciente = '{$GLOBALS["id_paciente"]}', nome_paciente = '{$GLOBALS["nome_paciente"]}' WHERE agendaMedica.id_agenda = '{$GLOBALS["id_agenda"]}'";
	
	$statement = $pdo->prepare($sql);

	// Executa a sentença já com os valores
	if($statement->execute()){	    
		$resultado = $statement->rowCount();
		if($resultado == 1) {
			echo '{"codid":1,"nome":"'.$GLOBALS["nome_paciente"].'"}';
		}
	} else {
	    // Definimos a mensagem de erro
	    echo 'Sistema Fora do Ar! Tente mais tarde...';; 
	} 

}//function Incluir()

function Excluir() {

	require_once('../config.php');
	
	// Prepara uma sentença para ser executada
	$sql = "UPDATE agendaMedica SET id_paciente = '', nome_paciente = '' WHERE agendaMedica.id_agenda = {$GLOBALS['id_agenda']}";
	$statement = $pdo->prepare($sql);

	// Executa a sentença já com os valores
	if($statement->execute()){	    
		$resultado = $statement->fetchAll(PDO::FETCH_ASSOC);
		//echo json_encode($resultado, JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);
		//print_r($resultado);  
		//echo ($resultado);  
		//print_r($resultado);
		echo '{"codid":1,"nome":"'.$GLOBALS["nome"].'","codigo":"'.$GLOBALS["id_paciente"].'"}';
	} else {
	    // Definimos a mensagem de erro
	    echo 'Sistema Fora do Ar! Tente mais tarde...';; 
	} 

}//function Excluir()

function Pesquisar() {
	require_once('../config.php');

	// Prepara uma sentença para ser executada
	$sql = "SELECT *  FROM agendaMedica WHERE id_paciente != '' AND nome_paciente LIKE '%{$GLOBALS["termo"]}%'";

	$statement = $pdo->prepare($sql);

	// Executa a sentença já com os valores
	if($statement->execute()){	    
		$resultado = $statement->fetchAll(PDO::FETCH_ASSOC);
		 
		echo json_encode($resultado, JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);
		//print_r($resultado);  
		//echo $resultado;
	} else {
	    // Definimos a mensagem de erro
	    echo 'Sistema Fora do Ar! Tente mais tarde...';; 
	} 

}//fim do Pesquisar

function ListarMedicos() {

    require_once('../config.php');

	// Prepara uma sentença para ser executada
	$sql = "SELECT id, nome, perfil, email FROM usuario WHERE perfil = 'ME'";

	$statement = $pdo->prepare($sql);

	// Executa a sentença já com os valores
	if($statement->execute()){	    
		$resultado = $statement->fetchAll(PDO::FETCH_ASSOC);
		 
		echo json_encode($resultado, JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);
		//print_r($resultado);  
		//echo $resultado;
	} else {
	    // Definimos a mensagem de erro
	    echo 'Sistema Fora do Ar! Tente mais tarde...';; 
	} 

}//fim do ListarMedicos

function ListarAgendaMedica() {
    require_once('../config.php');

	// Prepara uma sentença para ser executada
	$sql = "SELECT * FROM agendaMedica WHERE usuario_id = {$GLOBALS["idMedico"]} AND id_paciente = ''";

	$statement = $pdo->prepare($sql);

	// Executa a sentença já com os valores
	if($statement->execute()){	    
		$resultado = $statement->fetchAll(PDO::FETCH_ASSOC);
		 
		echo json_encode($resultado, JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);
		//print_r($resultado);  
		//echo $resultado;
	} else {
	    // Definimos a mensagem de erro
	    echo 'Sistema Fora do Ar! Tente mais tarde...';; 
	} 
}

RecuperaForm();

switch ($GLOBALS['modulo']) {
	case 'listaPacientes':
		ConsultaDados();
		break;
	case 'especifica':
		ConsultaEspecifica();
		break;
	case 'incluir':
		Incluir();
		break;	
	case 'excluir':
		Excluir();
		break;
	case 'listaAgenda':
		ListarAgendaMedica();
		break;	
	case 'pesquisar':
		Pesquisar();
        break;
    case 'listaMedica':
		ListarMedicos();
		break;	
	default:
    	//ConsultaDados();
		break;
}

?>