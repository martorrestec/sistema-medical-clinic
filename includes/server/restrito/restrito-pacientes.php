<?php

function RecuperaForm() {
	
	$post = $_REQUEST;

	// Verifica se todos campos vieram pelo formulário e se estão preenchidos
	if(isset($post['filtro']) || isset($post['valor']) || isset($post['modulo'])){
		foreach ($post as $key => $value) {
			$GLOBALS[$key] = filter_var($value);
        }//fim do foreach
        
	}//fim do if
    
}//fim do RecuperaForm

function ConsultaDados() {
	require_once('../config.php');
	
	// Prepara uma sentença para ser executada
	$sql = "SELECT * FROM paciente ORDER BY nome ASC LIMIT 0, 15";

	$statement = $pdo->prepare($sql);

	// Executa a sentença já com os valores
	if($statement->execute()){	    
		$resultado = $statement->fetchAll(PDO::FETCH_ASSOC);
        echo json_encode($resultado, JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);
        //print_r($resultado);  
		//echo $sql;
	} else {
	    // Definimos a mensagem de erro
	    echo 'Sistema Fora do Ar! Tente mais tarde...';; 
	} 

}//fim do ConsultaDados

function ConsultaEspecifica() {
	require_once('../config.php');

	// Prepara uma sentença para ser executada
	$sql = "SELECT * FROM paciente WHERE id = {$GLOBALS['id']}";

	$statement = $pdo->prepare($sql);

	// Executa a sentença já com os valores
	if($statement->execute()){	    
		$resultado = $statement->fetchAll(PDO::FETCH_ASSOC);
		 
		echo json_encode($resultado, JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);
		//print_r($resultado);  
		//echo $resultado;
	} else {
	    // Definimos a mensagem de erro
	    echo 'Sistema Fora do Ar! Tente mais tarde...';; 
	} 

}//fim do ConsultaEspecifica

function Incluir() {

	require_once('../config.php');
	
    // Prepara uma sentença para ser executada
    
    $sql = "INSERT INTO paciente (id, nome, email, cpf, identidade, dataNasc, estadocivil, sexo, telefone, nomePai, nomeMae, cep, endereco, numero, complemento, bairro, cidade, estado) VALUES (NULL, '{$GLOBALS["nome"]}', '{$GLOBALS["email"]}', '{$GLOBALS["cpf"]}', '{$GLOBALS["identidade"]}', '{$GLOBALS["dtNasc"]}', '{$GLOBALS["estCivil"]}', '{$GLOBALS["sexo"]}', '{$GLOBALS["telefone"]}', '{$GLOBALS["nomePai"]}', '{$GLOBALS["nomeMae"]}', '{$GLOBALS["cep"]}', '{$GLOBALS["endereco"]}', '{$GLOBALS["numero"]}', '{$GLOBALS["comple"]}', '{$GLOBALS["bairro"]}', '{$GLOBALS["cidade"]}', '{$GLOBALS["estado"]}')";

	$statement = $pdo->prepare($sql);

	// Executa a sentença já com os valores
	if($statement->execute()){	    
		$resultado = $statement->rowCount();
		if($resultado == 1) {
			echo '{"codid":1,"nome":"'.$GLOBALS["nome"].'"}';
		}
	} else {
	    // Definimos a mensagem de erro
	    echo 'Sistema Fora do Ar! Tente mais tarde...';; 
	} 

}//function Incluir()

function Alterar() {

	require_once('../config.php');
	
	// Prepara uma sentença para ser executada
	$sql = "UPDATE paciente SET nome = '{$GLOBALS["nome"]}', email = '{$GLOBALS["email"]}', cpf = '{$GLOBALS["cpf"]}', identidade = '{$GLOBALS["identidade"]}', dataNasc = '{$GLOBALS["dtNasc"]}', estadocivil = '{$GLOBALS["estCivil"]}', sexo = '{$GLOBALS["sexo"]}', telefone = '{$GLOBALS["telefone"]}', nomePai = '{$GLOBALS["nomePai"]}', nomeMae = '{$GLOBALS["nomeMae"]}', cep = '{$GLOBALS["cep"]}', endereco = '{$GLOBALS["endereco"]}', numero = '{$GLOBALS["numero"]}', complemento = '{$GLOBALS["comple"]}', bairro = '{$GLOBALS["bairro"]}', cidade = '{$GLOBALS["cidade"]}', estado = '{$GLOBALS["estado"]}' WHERE paciente.id = {$GLOBALS["id"]}";



	$statement = $pdo->prepare($sql);

	// Executa a sentença já com os valores
	if($statement->execute()){	    
		$resultado = $statement->rowCount();
		//echo json_encode($resultado, JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);
		//print_r($resultado);  
		//echo ($resultado);  
		//print_r($resultado);
		if($resultado == 1) {
			echo '{"codid":1,"nome":"'.$GLOBALS["nome"].'","codigo":"'.$GLOBALS["id"].'"}';
		}
	} else {
	    // Definimos a mensagem de erro
	    echo 'Sistema Fora do Ar! Tente mais tarde...';; 
	} 

}//function Alterar()

function Excluir() {

	require_once('../config.php');
	
	// Prepara uma sentença para ser executada
	$sql = "DELETE FROM paciente WHERE paciente.id = {$GLOBALS['id']}";
	$statement = $pdo->prepare($sql);

	// Executa a sentença já com os valores
	if($statement->execute()){	    
		$resultado = $statement->fetchAll(PDO::FETCH_ASSOC);
		//echo json_encode($resultado, JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);
		//print_r($resultado);  
		//echo ($resultado);  
		//print_r($resultado);
		echo '{"codid":1,"nome":"'.$GLOBALS["nome"].'","codigo":"'.$GLOBALS["id"].'"}';
	} else {
	    // Definimos a mensagem de erro
	    echo 'Sistema Fora do Ar! Tente mais tarde...';; 
	} 

}//function Excluir()

function Pesquisar() {
	require_once('../config.php');

	// Prepara uma sentença para ser executada
	$sql = "SELECT *  FROM paciente WHERE nome LIKE '%{$GLOBALS["termo"]}%'";

	$statement = $pdo->prepare($sql);

	// Executa a sentença já com os valores
	if($statement->execute()){	    
		$resultado = $statement->fetchAll(PDO::FETCH_ASSOC);
		 
		echo json_encode($resultado, JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);
		//print_r($resultado);  
		//echo $resultado;
	} else {
	    // Definimos a mensagem de erro
	    echo 'Sistema Fora do Ar! Tente mais tarde...';; 
	} 

}//fim do Pesquisar

RecuperaForm();

switch ($GLOBALS['modulo']) {
	case 'listaPacientes':
		ConsultaDados();
		break;
	case 'especifica':
		ConsultaEspecifica();
		break;
	case 'incluir':
		Incluir();
		break;	
	case 'alterar':
		Alterar();
		break;
	case 'excluir':
		Excluir();
		break;	
	case 'pesquisar':
		Pesquisar();
		break;	
	default:
    	//ConsultaDados();
		break;
}

?>