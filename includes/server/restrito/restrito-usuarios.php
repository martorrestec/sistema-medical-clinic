<?php

function RecuperaForm() {
	
	$post = $_REQUEST;

	// Verifica se todos campos vieram pelo formulário e se estão preenchidos
	if(isset($post['filtro']) || isset($post['valor']) || isset($post['modulo'])){
		foreach ($post as $key => $value) {
			$GLOBALS[$key] = filter_var($value);
        }//fim do foreach
	}//fim do if
    
}//fim do RecuperaForm

function ConsultaDados() {
	require_once('../config.php');
	
	// Prepara uma sentença para ser executada
	$sql = "SELECT *  FROM usuario ORDER BY nome ASC LIMIT 0, 15";

	$statement = $pdo->prepare($sql);

	// Executa a sentença já com os valores
	if($statement->execute()){	    
		$resultado = $statement->fetchAll(PDO::FETCH_ASSOC);
		echo json_encode($resultado, JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);
		//print_r($resultado);  
		//echo $sql;
	} else {
	    // Definimos a mensagem de erro
	    echo 'Sistema Fora do Ar! Tente mais tarde...';; 
	} 

}//fim do ConsultaDados

function ConsultaEspecifica() {
	require_once('../config.php');

	// Prepara uma sentença para ser executada
	$sql = "SELECT * FROM usuario WHERE id = {$GLOBALS['codigo']}";

	$statement = $pdo->prepare($sql);

	// Executa a sentença já com os valores
	if($statement->execute()){	    
		$resultado = $statement->fetchAll(PDO::FETCH_ASSOC);
		if ($GLOBALS['operacao'] == 'consultar' || $GLOBALS['operacao'] == 'alterar') {
			$resultado[0]['senha']='';
		} 
		echo json_encode($resultado, JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);
		//print_r($resultado);  
		//echo $resultado;
	} else {
	    // Definimos a mensagem de erro
	    echo 'Sistema Fora do Ar! Tente mais tarde...';; 
	} 

}//fim do ConsultaEspecifica

function Incluir() {

	require_once('../config.php');
	
    // Prepara uma sentença para ser executada
    
    $sql = "INSERT INTO usuario (id, nome, email, cargo, login, senha, dataCad, perfil, turnoTrab, diasTrab) VALUES (NULL, '{$GLOBALS["nome"]}', '{$GLOBALS["email"]}', '{$GLOBALS["cargo"]}', '{$GLOBALS["login"]}', '".md5($GLOBALS["senha"])."', '{$GLOBALS["dataCad"]}', '{$GLOBALS["perfil"]}', '{$GLOBALS["turnoTrab"]}', '{$GLOBALS["diasTrab"]}')";

	$statement = $pdo->prepare($sql);

	// Executa a sentença já com os valores
	if($statement->execute()){	    
		$resultado = $statement->rowCount();
		if($resultado == 1) {
			echo '{"codid":1,"nome":"'.$GLOBALS["nome"].'"}';
		}
	} else {
	    // Definimos a mensagem de erro
	    echo 'Sistema Fora do Ar! Tente mais tarde...';; 
	} 

}//function Incluir()

function Alterar() {

	require_once('../config.php');
	
	// Prepara uma sentença para ser executada
	$sql = "UPDATE usuario SET nome = '{$GLOBALS["nome"]}', email = '{$GLOBALS["email"]}', cargo = '{$GLOBALS["cargo"]}', login = '{$GLOBALS["login"]}', senha = '".md5($GLOBALS["senha"])."', dataCad = '{$GLOBALS["dataCad"]}', perfil = '{$GLOBALS["perfil"]}', turnoTrab = '{$GLOBALS["turnoTrab"]}', diasTrab = '{$GLOBALS["diasTrab"]}' WHERE usuario.id = '{$GLOBALS["id"]}'";
	//echo $GLOBALS['senha'];

	$statement = $pdo->prepare($sql);

	// Executa a sentença já com os valores
	if($statement->execute()){	    
		$resultado = $statement->rowCount();
		//echo json_encode($resultado, JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);
		//print_r($resultado);  
		//echo ($resultado);  
		//print_r($resultado);
		if($resultado == 1) {
			echo '{"codid":1,"nome":"'.$GLOBALS["nome"].'","codigo":"'.$GLOBALS["id"].'"}';
		}
	} else {
	    // Definimos a mensagem de erro
	    echo 'Sistema Fora do Ar! Tente mais tarde...';; 
	} 

}//function Alterar()

function Excluir() {

	require_once('../config.php');
	
	// Prepara uma sentença para ser executada
	$sql = "DELETE FROM usuario WHERE usuario.id = {$GLOBALS['id']}";
	$statement = $pdo->prepare($sql);

	// Executa a sentença já com os valores
	if($statement->execute()){	    
		$resultado = $statement->fetchAll(PDO::FETCH_ASSOC);
		//echo json_encode($resultado, JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);
		//print_r($resultado);  
		//echo ($resultado);  
		//print_r($resultado);
		echo '{"codid":1,"nome":"'.$GLOBALS["nome"].'","codigo":"'.$GLOBALS["id"].'"}';
	} else {
	    // Definimos a mensagem de erro
	    echo 'Sistema Fora do Ar! Tente mais tarde...';; 
	} 

}//function Excluir()

function Pesquisar() {
	require_once('../config.php');

	// Prepara uma sentença para ser executada
	$sql = "SELECT *  FROM usuario WHERE nome LIKE '%{$GLOBALS["termo"]}%'";

	$statement = $pdo->prepare($sql);

	// Executa a sentença já com os valores
	if($statement->execute()){	    
		$resultado = $statement->fetchAll(PDO::FETCH_ASSOC);
		 
		echo json_encode($resultado, JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);
		//print_r($resultado);  
		//echo $resultado;
	} else {
	    // Definimos a mensagem de erro
	    echo 'Sistema Fora do Ar! Tente mais tarde...';; 
	} 

}//fim do ConsultaEspecifica

RecuperaForm();

switch ($GLOBALS['modulo']) {
	case 'listaUsuarios':
		ConsultaDados();
		break;
	case 'especifica':
		ConsultaEspecifica();
		break;
	case 'incluir':
		Incluir();
		break;	
	case 'alterar':
		Alterar();
		break;
	case 'excluir':
		Excluir();
		break;
	case 'pesquisar':
		Pesquisar();
		break;	
	default:
    	//ConsultaDados();
		break;
}





?>