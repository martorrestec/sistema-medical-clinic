<?php

function RecuperaForm() {
	
	$post = $_REQUEST;

	// Verifica se todos campos vieram pelo formulário e se estão preenchidos
	if(isset($post['filtro']) || isset($post['valor']) || isset($post['modulo'])){
		foreach ($post as $key => $value) {
			$GLOBALS[$key] = filter_var($value);
        }//fim do foreach
        
	}//fim do if
    
}//fim do RecuperaForm

function ConsultaDados() {
	require_once('../config.php');
	
	// Prepara uma sentença para ser executada
	$sql = "SELECT * FROM medicamento ORDER BY nomeGenerico ASC LIMIT 0, 15";

	$statement = $pdo->prepare($sql);

	// Executa a sentença já com os valores
	if($statement->execute()){	    
		$resultado = $statement->fetchAll(PDO::FETCH_ASSOC);
        echo json_encode($resultado, JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);
        //print_r($resultado);  
		//echo $sql;
	} else {
	    // Definimos a mensagem de erro
	    echo 'Sistema Fora do Ar! Tente mais tarde...';; 
	} 

}//fim do ConsultaDados

function ConsultaEspecifica() {
	require_once('../config.php');

	// Prepara uma sentença para ser executada
	$sql = "SELECT * FROM medicamento WHERE id = {$GLOBALS['id']}";

	$statement = $pdo->prepare($sql);

	// Executa a sentença já com os valores
	if($statement->execute()){	    
		$resultado = $statement->fetchAll(PDO::FETCH_ASSOC);
		 
		echo json_encode($resultado, JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);
		//print_r($resultado);  
		//echo $resultado;
	} else {
	    // Definimos a mensagem de erro
	    echo 'Sistema Fora do Ar! Tente mais tarde...';; 
	} 

}//fim do ConsultaEspecifica

function Incluir() {

	require_once('../config.php');
	
    // Prepara uma sentença para ser executada
    
    $sql = "INSERT INTO medicamento (id, nomeGenerico, nomeFabrica, formaFarmaucetica, fabricante, concentracao) VALUES (NULL, '{$GLOBALS["nomeGenerico"]}', '{$GLOBALS["nomeFabrica"]}', '{$GLOBALS["formaFarma"]}', '{$GLOBALS["fabricante"]}', '{$GLOBALS["concentracao"]}')";

	$statement = $pdo->prepare($sql);

	// Executa a sentença já com os valores
	if($statement->execute()){	    
		$resultado = $statement->rowCount();
		if($resultado == 1) {
			echo '{"codid":1,"nomeFabrica":"'.$GLOBALS["nomeFabrica"].'"}';
		}
	} else {
	    // Definimos a mensagem de erro
	    echo 'Sistema Fora do Ar! Tente mais tarde...';; 
	} 

}//function Incluir()

function Alterar() {

	require_once('../config.php');
	
	// Prepara uma sentença para ser executada
	$sql = "UPDATE medicamento SET nomeGenerico = '{$GLOBALS["nomeGenerico"]}', nomeFabrica = '{$GLOBALS["nomeFabrica"]}', formaFarmaucetica = '{$GLOBALS["formaFarmaucetica"]}', fabricante = '{$GLOBALS["fabricante"]}', concentracao = '{$GLOBALS["concentracao"]}' WHERE medicamento.id = {$GLOBALS["id"]}";


	$statement = $pdo->prepare($sql);

	// Executa a sentença já com os valores
	if($statement->execute()){	    
		$resultado = $statement->rowCount();
		//echo json_encode($resultado, JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);
		//print_r($resultado);  
		//echo ($resultado);  
		//print_r($resultado);
		if($resultado == 1) {
			echo '{"codid":1,"nomeGenerico":"'.$GLOBALS["nomeGenerico"].'","codigo":"'.$GLOBALS["id"].'"}';
		}
	} else {
	    // Definimos a mensagem de erro
	    echo 'Sistema Fora do Ar! Tente mais tarde...';; 
	} 

}//function Alterar()

function Excluir() {

	require_once('../config.php');
	
	// Prepara uma sentença para ser executada
	$sql = "DELETE FROM usuario WHERE usuario.id = {$GLOBALS['id']}";
	$statement = $pdo->prepare($sql);

	// Executa a sentença já com os valores
	if($statement->execute()){	    
		$resultado = $statement->fetchAll(PDO::FETCH_ASSOC);
		//echo json_encode($resultado, JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);
		//print_r($resultado);  
		//echo ($resultado);  
		//print_r($resultado);
		echo '{"codid":1,"nome":"'.$GLOBALS["nome"].'","codigo":"'.$GLOBALS["id"].'"}';
	} else {
	    // Definimos a mensagem de erro
	    echo 'Sistema Fora do Ar! Tente mais tarde...';; 
	} 

}//function Excluir()

function Pesquisar() {
	require_once('../config.php');

	// Prepara uma sentença para ser executada
	$sql = "SELECT *  FROM medicamento WHERE nomeGenerico LIKE '%{$GLOBALS["termo"]}%'";

	$statement = $pdo->prepare($sql);

	// Executa a sentença já com os valores
	if($statement->execute()){	    
		$resultado = $statement->fetchAll(PDO::FETCH_ASSOC);
		 
		echo json_encode($resultado, JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);
		//print_r($resultado);  
		//echo $resultado;
	} else {
	    // Definimos a mensagem de erro
	    echo 'Sistema Fora do Ar! Tente mais tarde...';; 
	} 

}//fim do Pesquisar

RecuperaForm();

switch ($GLOBALS['modulo']) {
	case 'listaMedicamentos':
		ConsultaDados();
		break;
	case 'especifica':
		ConsultaEspecifica();
		break;
	case 'incluir':
		Incluir();
		break;	
	case 'alterar':
		Alterar();
		break;
	case 'excluir':
		Excluir();
		break;	
	case 'pesquisar':
		Pesquisar();
		break;	
	default:
    	//ConsultaDados();
		break;
}

?>