<?php

function RecuperaForm() {
	
	$post = $_REQUEST;

	// Verifica se todos campos vieram pelo formulário e se estão preenchidos
	if(isset($post['filtro']) || isset($post['valor']) || isset($post['modulo'])){
		foreach ($post as $key => $value) {
			$GLOBALS[$key] = filter_var($value);
        }//fim do foreach
        
	}//fim do if
    
}//fim do RecuperaForm

function ConsultaDados() {
	require_once('../config.php');
	
	// Prepara uma sentença para ser executada
	$sql = "SELECT id, nome, cargo, turnoTrab, diasTrab FROM usuario WHERE perfil = 'ME' ORDER BY nome ASC";

	$statement = $pdo->prepare($sql);

	// Executa a sentença já com os valores
	if($statement->execute()){	    
		$resultado = $statement->fetchAll(PDO::FETCH_ASSOC);
        echo json_encode($resultado, JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);
        //print_r($resultado);  
		//echo $sql;
	} else {
	    // Definimos a mensagem de erro
	    echo 'Sistema Fora do Ar! Tente mais tarde...';; 
	} 

}//fim do ConsultaDados

function Incluir() {

	require_once('../config.php');
	
    // Prepara uma sentença para ser executada
    
    $sql = "INSERT INTO agendaMedica (id_agenda, usuario_id, nome_medico, data, id_paciente, nome_paciente) VALUES (NULL, '{$GLOBALS["id"]}', '{$GLOBALS["nome_medico"]}', '{$GLOBALS["data"]}', '', '');";

	$statement = $pdo->prepare($sql);

	// Executa a sentença já com os valores
	if($statement->execute()){	    
		$resultado = $statement->rowCount();
		if($resultado == 1) {
			echo '{"codid":1,"nome":"'.$GLOBALS["nome_medico"].'"}';
		}
	} else {
	    // Definimos a mensagem de erro
	    echo 'Sistema Fora do Ar! Tente mais tarde...';; 
	} 

}//function Incluir()

function Excluir() {

	require_once('../config.php');
	
	// Prepara uma sentença para ser executada
	$sql = "DELETE FROM agendaMedica WHERE id_agenda = {$GLOBALS['id_agenda']}";
	$statement = $pdo->prepare($sql);

	// Executa a sentença já com os valores
	if($statement->execute()){	    
		$resultado = $statement->fetchAll(PDO::FETCH_ASSOC);
		//echo json_encode($resultado, JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);
		//print_r($resultado);  
		//echo ($resultado);  
		//print_r($resultado);
		echo '{"codid":1, "codigo":"'.$GLOBALS["id_agenda"].'"}';
	} else {
	    // Definimos a mensagem de erro
	    echo 'Sistema Fora do Ar! Tente mais tarde...';; 
	} 

}//function Excluir()

function Pesquisar() {
	require_once('../config.php');

	// Prepara uma sentença para ser executada
	$sql = "SELECT * FROM usuario WHERE nome LIKE '%{$GLOBALS["termo"]}%' AND perfil = 'ME' ORDER BY nome ASC";

	$statement = $pdo->prepare($sql);

	// Executa a sentença já com os valores
	if($statement->execute()){	    
		$resultado = $statement->fetchAll(PDO::FETCH_ASSOC);
		 
		echo json_encode($resultado, JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);
		//print_r($resultado);  
		//echo $resultado;
	} else {
	    // Definimos a mensagem de erro
	    echo 'Sistema Fora do Ar! Tente mais tarde...';; 
	} 

}//fim do Pesquisar

function BuscaMedico() {

    require_once('../config.php');

	// Prepara uma sentença para ser executada
	$sql = "SELECT id, nome, cargo, turnoTrab, diasTrab FROM usuario WHERE id = '{$GLOBALS["id"]}' AND perfil = 'ME'";

	$statement = $pdo->prepare($sql);

	// Executa a sentença já com os valores
	if($statement->execute()){	    
		$resultado = $statement->fetchAll(PDO::FETCH_ASSOC);		 
		echo json_encode($resultado, JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);
	} else {
	    // Definimos a mensagem de erro
	    echo 'Sistema Fora do Ar! Tente mais tarde...';; 
	} 

}//fim do BuscaMedico

function BuscaAgendaMedico() {

    require_once('../config.php');

	// Prepara uma sentença para ser executada
	$sql = "SELECT * FROM agendaMedica WHERE usuario_id = '{$GLOBALS["id"]}' ORDER BY data ASC";

	$statement = $pdo->prepare($sql);

	// Executa a sentença já com os valores
	if($statement->execute()){	    
		$resultado = $statement->fetchAll(PDO::FETCH_ASSOC); 
		echo json_encode($resultado, JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);
	} else {
	    // Definimos a mensagem de erro
	    echo 'Sistema Fora do Ar! Tente mais tarde...';; 
	} 

}//fim do BuscaAgendaMedico


function BuscaAgendaPaciente() {
	require_once('../config.php');

	// Prepara uma sentença para ser executada
	$sql = "SELECT * FROM agendaMedica WHERE id_paciente = '{$GLOBALS["id"]}' ORDER BY data ASC";

	$statement = $pdo->prepare($sql);

	// Executa a sentença já com os valores
	if($statement->execute()){	    
		$resultado = $statement->fetchAll(PDO::FETCH_ASSOC); 
		echo json_encode($resultado, JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);
	} else {
	    // Definimos a mensagem de erro
	    echo 'Sistema Fora do Ar! Tente mais tarde...';; 
	} 
}//fim do BuscaAgendaPaciente


RecuperaForm();

switch ($GLOBALS['modulo']) {
	case 'listaMedicos':
		ConsultaDados();
        break;
    case 'buscaMedico':
		BuscaMedico();
		break;
	case 'pesquisar':
		Pesquisar();
        break;	
    case 'buscaAgenda':
        BuscaAgendaMedico();
		break;
	case 'buscaAgendaPaciente':
        BuscaAgendaPaciente();
        break;
    case 'incluir':
        Incluir();
        break;
    case 'excluir':
        Excluir();
        break;
	default:
    	//ConsultaDados();
		break;
}

?>