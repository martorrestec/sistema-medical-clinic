$(document).ready(function(){
    //Controlo a exibição dos campos de horário do medicos
    CarregaMedicamentos();
    
    $('.pesquisar').on('click',function(){
        var linhaCarregando = $("<tr><td class='loading text-center' colspan='5'><img width='15' class='mr-2' src='../includes/image/ajax-loader.gif'/><span>Carregando...</span></td></tr>");
        var termo = $('.txtPesquisar').val();

        $.ajax({
            url : '../includes/server/restrito/restrito-medicamentos.php',
            dataType : 'json',
            type : 'POST',
            data : {'modulo' : 'pesquisar', 'termo' : termo},
            beforeSend : function(){
                $('#linhaMedicamento').html('');
            	$('#linhaMedicamento').append(linhaCarregando);
            },
            complete : function(){
            	$(linhaCarregando).remove();
            },
            success : function(dados, textStatus){
                console.log(dados);
                if(dados.length != 0) {
                    $('#linhaMedicamento').html('');
                    CarregaLinhaTabela(dados);
                } else {
                    $('#linhaMedicamento').html('<tr><td class="loading text-center" colspan="5">Pesquisa sem resultados.</td></tr>');
                }
                
            }
        });
    });

    //Chamada para os módulos de Modal
    $('.add-medicamentos').click(function(){
        //console.log('clicou');
        $('.modal-medicamentos .modal-title').html('Cadastrar Medicamentos');
        $('.modal-medicamentos .btn-action').html('Cadastrar');
        $('.modal-medicamentos form').trigger("reset");
        //$('.modal-medicamento [name*="txt"]').attr('disabled',false);
        //$('#codigo, #dtCad').attr('disabled',true);
        $('.incluirMedicamento').removeClass('d-none');
        $('.consultarMedicamento, .alterarMedicamento').addClass('d-none');
        $('.modal-medicamentos').modal('show');
    });
    
    $('.incluirMedicamento').on('click',function(){
        
        codigo = $('#codigo').val();
        nomeGenerico = $('#nomeGenerico').val();
        nomeFabrica = $('#nomeFabrica').val();
        formaFarma = $('#formaFarma').val();
        fabricante = $('#fabricante').val();
        concentracao = $('#concentracao').val();
        
        if( nomeGenerico == '' || nomeFabrica == '' || formaFarma == '' || fabricante == '' || concentracao == '') {
            alert('Verifique o preenchimento de todos os campos!')
            return false;
        }

        $.ajax({
            url : '../includes/server/restrito/restrito-medicamentos.php',
            dataType : 'text',
            type : 'POST',
            data : {
                'modulo' : 'incluir', 'id': codigo, 'nomeGenerico' : nomeGenerico, 'nomeFabrica' : nomeFabrica, 'formaFarma' : formaFarma, 'concentracao' : concentracao, 'fabricante' : fabricante },
            success : function(dados, textStatus){
                dados = JSON.parse(dados);
                alert(dados.nomeFabrica +' cadastrado com sucesso!');
                $('.modal-medicamentos').modal('hide');
                $('.modal-medicamentos form').trigger("reset");
                CarregaMedicamentos();
            }
        });
    });

    $('.alterarMedicamento').on('click', function(){
        //console.log('vamos alterar');

        codigo = $('#codigo').val();
        nomeGenerico = $('#nomeGenerico').val();
        nomeFabrica = $('#nomeFabrica').val();
        formaFarma = $('#formaFarma').val();
        fabricante = $('#fabricante').val();
        concentracao = $('#concentracao').val();
        
        if( nomeGenerico == '' || nomeFabrica == '' || formaFarma == '' || fabricante == '' || concentracao == '') {
            alert('Verifique o preenchimento de todos os campos!')
            return false;
        }
        $.ajax({
            url : '../includes/server/restrito/restrito-medicamentos.php',
            dataType : 'text',
            type : 'POST',
            data : {
                'modulo' : 'alterar', 'id': codigo, 'nomeGenerico' : nomeGenerico, 'nomeFabrica' : nomeFabrica, 'formaFarmaucetica' : formaFarma, 'concentracao' : concentracao, 'fabricante' : fabricante },
            success : function(dados, textStatus){
                dados = JSON.parse(dados);
                console.log(dados);
                alert(dados.nomeGenerico +' alterado com sucesso!');
                $('.modal-medicamentos').modal('hide');
                $('.modal-medicamentos form').trigger("reset");
                CarregaMedicamentos();
            }
        });

    });

    $('.excluirMedicamento').on('click', function(){

        codigo = $('.modal-excluir-dialog .nomeMedicamento').attr('data-codigo');
        nome = $('.modal-excluir-dialog .nomeMedicamento').html();

        $.ajax({
            url : '../includes/server/restrito/restrito-medicamentos.php',
            dataType : 'text',
            type : 'POST',
            data : { 'modulo' : 'excluir', 'id': codigo, 'nome':nome },
            success : function(dados, textStatus){
                dados = JSON.parse(dados);
                //console.log(dados);
                alert(dados.nome +' excluído com sucesso!');
                $('.modal-excluir-dialog').modal('hide');
                $('#tr_' + codigo).remove();                
            }
        });
    });
});    

function ConsultaMedicamento(codigo, operacao) {

    //console.log(codigo);
    $.ajax({
        url : '../includes/server/restrito/restrito-medicamentos.php',
        dataType : 'json',
        type : 'POST',
        data : { 'modulo' : 'especifica', 'operacao' : operacao, 'id': codigo },
        success : function(dados, textStatus){
            //dados = JSON.parse(dados);
            console.log(dados);
            $('.modal-medicamento form').trigger("reset");
            $('#codigo').val(dados[0].id);
            $('#nomeGenerico').val(dados[0].nomeGenerico);
            $('#nomeFabrica').val(dados[0].nomeFabrica);
            $('#fabricante').val(dados[0].fabricante);
            $('#concentracao').val(dados[0].concentracao);
            $('#formaFarma').val(dados[0].formaFarmaucetica);                                   
        }
    });
}//fim do document ready

function CarregaMedicamentos() {
    var linhaCarregando = $("<tr><td class='loading text-center' colspan='5'><img width='15' class='mr-2' src='../includes/image/ajax-loader.gif'/><span>Carregando...</span></td></tr>");
    
	$.ajax({

		url : '../includes/server/restrito/restrito-medicamentos.php',
		dataType : 'json',
		type : 'POST',
        data : {'modulo' : 'listaMedicamentos'},
        beforeSend : function(){
            $('#linhaMedicamento').html('');
            $('#linhaMedicamento').append(linhaCarregando);
        },
        complete : function(){
            $(linhaCarregando).remove();
        },
		success : function(dados, textStatus){
            //console.log(dados);
            $('#linhaMedicamento').html('');
            CarregaLinhaTabela(dados);
		}
	});

}//fim do CarregaMedicamentos()

function CarregaLinhaTabela(medicamento) {

    var linha = '';

    for(var i=0; i<medicamento.length; i++) {
        linha+='<tr id="tr_'+medicamento[i].id+'"><td class="text-center">'+medicamento[i].id+'</td><td>'+medicamento[i].nomeGenerico+'</td><td>'+medicamento[i].nomeFabrica+'</td><td class="text-center">'+medicamento[i].fabricante+'</td><td class="text-center"><button data-codigo="'+medicamento[i].id+'" class="border-0 bg-transparent modal-consultar" type="button" title="Consultar '+medicamento[i].nomeFabrica+'"><i class="fas fa-capsules text-blue"></i></button><button data-codigo="'+medicamento[i].id+'" class="border-0 bg-transparent modal-alterar" type="button" title="Alterar '+medicamento[i].nomeFabrica+'"><i class="fas fa-edit text-green"></i></button><button data-codigo="'+medicamento[i].id+'" data-nome="'+medicamento[i].nomeGenerico+'" class="border-0 bg-transparent modal-excluir" type="button" title="Excluir '+medicamento[i].nomeFabrica+'"><i class="fas fa-times text-red"></i></button></td>';
    }

    $('#linhaMedicamento').html(linha);

    //adiciono os eventos no demais botões
    $('.modal-consultar').click(function(){
        $('.modal-medicamentos .modal-title').html('Consultar Medicamento');
        $('.modal-medicamentos .btn-action').html('Consultar');
        $('.modal-medicamentos [name*="txt"]').attr('disabled',true);
        ConsultaMedicamento($(this).attr('data-codigo'), 'consultar');
        $('.consultarMedicamento, .incluirMedicamento, .alterarMedicamento').addClass('d-none');
        $('.modal-medicamentos').modal('show');
    });
    
    $('.modal-alterar').click(function(){
        $('.modal-medicamentos .modal-title').html('Alterar Medicamento');
        $('.modal-medicamentos .btn-action').html('Alterar');
        $('.modal-medicamentos [name*="txt"]').attr('disabled',false);
        $('#codigo, #dtCad').attr('disabled',true);
        ConsultaMedicamento($(this).attr('data-codigo'), 'alterar');
        $('.alterarMedicamento').removeClass('d-none');
        $('.consultarMedicamento, .incluirMedicamento').addClass('d-none');
        $('.modal-medicamentos').modal('show');
    });

    $('.modal-excluir').click(function(){
        $('.modal-excluir-dialog .nomeMedicamento').html($(this).attr('data-nome'));
        $('.modal-excluir-dialog .nomeMedicamento').attr('data-codigo',$(this).attr('data-codigo'));
        $('.modal-excluir-dialog').modal('show');
    });

    

}//fim do CarregaLinhaTabela

