$(document).ready(function(){
    //Controlo a exibição dos campos de horário do medicos
    CarregaMedicos();
    
    $('.pesquisar').on('click',function(){
        var linhaCarregando = $("<tr><td class='loading text-center' colspan='5'><img width='15' class='mr-2' src='../includes/image/ajax-loader.gif'/><span>Carregando...</span></td></tr>");
        var termo = $('.txtPesquisar').val();

        $.ajax({
            url : '../includes/server/restrito/restrito-agenda.php',
            dataType : 'json',
            type : 'POST',
            data : {'modulo' : 'pesquisar', 'termo' : termo},
            beforeSend : function(){
                $('#linhaAgenda').html('');
            	$('#linhaAgenda').append(linhaCarregando);
            },
            complete : function(){
            	$(linhaCarregando).remove();
            },
            success : function(dados, textStatus){
                //console.log(dados);
                if(dados.length != 0) {
                    $('#linhaAgenda').html('');
                    CarregaLinhaTabela(dados);
                } else {
                    $('#linhaAgenda').html('<tr><td class="loading text-center" colspan="5">Pesquisa sem resultados.</td></tr>');
                }
                
            }
        });
    });

    $('.incluirAgenda').on('click',function(){
        idMedico = $(this).attr('data-idMedico'); 
        nomeMedico = $(this).attr('data-nomeMedico'); 
        dataHora = $('#dataAgend').val()+' '+$('#horaAgend').val()+':00';
        console.log(dataHora);
        $.ajax({
            url : '../includes/server/restrito/restrito-agenda.php',
            dataType : 'json',
            type : 'POST',
            data : {'modulo' : 'incluir', 'nome_medico' : nomeMedico, 'id' : idMedico, 'data' : dataHora},
            success : function(dados, textStatus){
                console.log(dados);
                alert('Agenda incluída com sucesso');
                ConsultaAgendaMedico(idMedico, 'incluir');
            }
        });
    });
    
});//fim do document ready    

//Carrego os dados do Medico no Modal
function ConsultaDadosMedico(codigo, operacao) {
    //console.log(codigo);
    $.ajax({
        url : '../includes/server/restrito/restrito-agenda.php',
        dataType : 'json',
        type : 'POST',
        data : { 'modulo' : 'buscaMedico', 'operacao' : operacao, 'id': codigo },
        success : function(dados, textStatus){
            //dados = JSON.parse(dados);
            //console.log(dados);
            $('.nomeMedico').html(dados[0].nome);
            $('.diasAtend').html(dados[0].diasTrab);
            $('.turnoAtend').html(dados[0].turnoTrab);                                         
        }
    });
}//fim do ConsultaDadosMedico

//Carrego os Dados dos Medicos na Lista Principal
function CarregaMedicos() {
    var linhaCarregando = $("<tr><td class='loading text-center' colspan='5'><img width='15' class='mr-2' src='../includes/image/ajax-loader.gif'/><span>Carregando...</span></td></tr>");
    
	$.ajax({

		url : '../includes/server/restrito/restrito-agenda.php',
		dataType : 'json',
		type : 'POST',
        data : {'modulo' : 'listaMedicos'},
        beforeSend : function(){
            $('#linhaAgenda').html('');
            $('#linhaAgenda').append(linhaCarregando);
        },
        complete : function(){
            $(linhaCarregando).remove();
        },
		success : function(dados, textStatus){
            console.log(dados);
            $('#linhaAgenda').html('');
            // $('.incluirAgenda').attr('data-idmedico',dados[0].id);
            // $('.incluirAgenda').attr('data-nomemedico',dados[0].nome);
            CarregaLinhaTabela(dados);
		}
	});

}//fim do CarregaMedicos()

//Carrego a agenda do Medico no Modal
function ConsultaAgendaMedico(codigo, operacao) {
    $('#linhaConsulta').html(''); 
    $.ajax({
        url : '../includes/server/restrito/restrito-agenda.php',
        dataType : 'json',
        type : 'POST',
        data : { 'modulo' : 'buscaAgenda', 'operacao' : operacao, 'id': codigo },
        success : function(dados, textStatus){
            //dados = JSON.parse(dados);
            console.log(dados);            
            var linha = '';
            for(var i=0; i<dados.length; i++) {
                linha+='<tr id="tr_ag_'+dados[i].id_agenda+'"><td class="text-center">'+dados[i].id_agenda+'</td><td>'+dados[i].nome_medico+'</td><td class="text-center">'+FormataDataMysql(dados[i].data.split(' ')[0])+'</td><td class="text-center">'+dados[i].data.split(' ')[1].substr(0,5)+'</td><td>'+dados[i].nome_paciente+'</td><td class="text-center"><button data-codigo="'+dados[i].id_agenda+'" data-nome="'+dados[i].nome_paciente+'" data-dataAgend="'+dados[i].data+'" class="border-0 bg-transparent excluirAgenda" type="button" title="Excluir '+dados[i].id_agenda+'"><i class="fas fa-times text-red"></i></button></td></tr>';
            }
            $('#linhaConsulta').html(linha);   
            
            $('.excluirAgenda').on('click', function(){
                data = $(this).attr('data-dataAgend');
                codigo = $(this).attr('data-codigo');
                
                if(confirm('Deseja excluir o agendamento de '+data+' ?')) {
                    ExcluirAgendamento(codigo);
                }
            });
        }
    });
} // fim do ConsultaAgendaMedico

function ExcluirAgendamento(codigo) {

    $.ajax({
		url : '../includes/server/restrito/restrito-agenda.php',
		dataType : 'json',
		type : 'POST',
        data : {'modulo' : 'excluir', 'id_agenda' : codigo },
		success : function(dados, textStatus){
            //console.log(dados);
            $('#tr_ag_'+dados.codigo).remove();
		}
	});

}//fim do ExcluirAgendamento

function CarregaLinhaTabela(medico) {

    var linha = '';

    for(var i=0; i<medico.length; i++) {
        linha+='<tr id="tr_'+medico[i].id+'"><td class="text-center">'+medico[i].id+'</td><td>'+medico[i].nome+'</td><td>'+medico[i].cargo+'</td><td class="text-center"><button data-nomemedico="'+medico[i].nome+'" data-codigo="'+medico[i].id+'" class="border-0 btn btn-sm btn-success modal-agenda" type="button" title="Adicionar Agenda de '+medico[i].nome+'"><i class="fas fa-calendar-alt mr-2"></i>Agenda</td></tr>';
    }

    $('#linhaAgenda').html(linha);

    //adiciono os eventos no demais botões
    $('.modal-agenda').click(function(){
        $('.modal-add form').trigger('reset');
        $('.incluirAgenda').attr('data-idmedico',$(this).attr('data-codigo'));
        $('.incluirAgenda').attr('data-nomemedico',$(this).attr('data-nomemedico'));
        ConsultaDadosMedico($(this).attr('data-codigo'), 'consultar');
        ConsultaAgendaMedico($(this).attr('data-codigo'), 'consultar');
        
        $('.modal-add').modal('show');
    });    

}//fim do CarregaLinhaTabela

function ConverteDataMysql(data) {
    dataNova = data.split('-');
    dataMysql = dataNova[2]+'-'+dataNova[1]+'-'+dataNova[0];
    return (dataMysql);
} //fim da ConverteDataMysql
function FormataDataMysql(data) {
    dataNova = data.split('-');
    dataMysql = dataNova[2]+'/'+dataNova[1]+'/'+dataNova[0];
    return (dataMysql);s
} //fim da ConverteDataMysql

