$(document).ready(function() {

    CarregaPacientes();
    
    $('.pesquisar').on('click',function(){
        var linhaCarregando = $("<tr><td class='loading text-center' colspan='5'><img width='15' class='mr-2' src='../includes/image/ajax-loader.gif'/><span>Carregando...</span></td></tr>");
        var termo = $('.txtPesquisar').val();

        $.ajax({
            url : '../includes/server/restrito/restrito-consultas.php',
            dataType : 'json',
            type : 'POST',
            data : {'modulo' : 'pesquisar', 'termo' : termo},
            beforeSend : function(){
                $('#linhaConsulta').html('');
            	$('#linhaConsulta').append(linhaCarregando);
            },
            complete : function(){
            	$(linhaCarregando).remove();
            },
            success : function(dados, textStatus){
                //console.log(dados);
                if(dados.length != 0) {
                    $('#linhaConsulta').html('');
                    CarregaLinhaTabela(dados);
                } else {
                    $('#linhaConsulta').html('<tr><td class="loading text-center" colspan="5">Pesquisa sem resultados.</td></tr>');
                }
                
            }
        });
    });

    $('.excluirConsulta').on('click', function(){

        codigo = $('.modal-excluir-dialog .nomePaciente').attr('data-codigo');
        id_agenda = $('.modal-excluir-dialog .nomePaciente').attr('data-idAgenda');
        nome = $('.modal-excluir-dialog .nomePaciente').html();

        $.ajax({
            url : '../includes/server/restrito/restrito-consultas.php',
            dataType : 'text',
            type : 'POST',
            data : { 'modulo' : 'excluir', 'id_paciente': codigo, 'nome' : nome, 'id_agenda' : id_agenda },
            success : function(dados, textStatus){
                dados = JSON.parse(dados);
                console.log(dados);
                alert(dados.nome +' excluído com sucesso!');
                $('.modal-excluir-dialog').modal('hide');
                $('#tr_' + codigo).remove();                
            }
        });
    });

});

function CarregaPacientes() {
    var linhaCarregando = $("<tr><td class='loading text-center' colspan='5'><img width='15' class='mr-2' src='../includes/image/ajax-loader.gif'/><span>Carregando...</span></td></tr>");
    
	$.ajax({

		url : '../includes/server/restrito/restrito-consultas.php',
		dataType : 'json',
		type : 'POST',
        data : {'modulo' : 'listaPacientes'},
        beforeSend : function(){
            $('#linhaConsulta').html('');
            $('#linhaConsulta').append(linhaCarregando);
        },
        complete : function(){
            $(linhaCarregando).remove();
        },
		success : function(dados, textStatus){
            //console.log(dados);
            $('#linhaConsulta').html('');
            CarregaLinhaTabela(dados);
		}
	});

}//fim do CarregaExames()

function CarregaLinhaTabela(paciente) {

    var linha = '';
    
    for(var i=0; i<paciente.length; i++) {
        linha+='<tr id="tr_'+paciente[i].id_paciente+'"><td class="text-center">'+paciente[i].id_paciente+'</td><td>'+paciente[i].nome_paciente+'</td><td>'+paciente[i].nome_medico+'</td><td class="text-center">'+paciente[i].data+'</td><td class="text-center"><button data-codigo="'+paciente[i].id_paciente+'" data-nome="'+paciente[i].nome_paciente+'"  data-idAgenda="'+paciente[i].id_agenda+'" class="border-0 bg-transparent modal-excluir" type="button" title="Excluir Consulta Marl Arnold"><i class="fas fa-calendar-times text-red"></i></button></td></tr>';
    }

    $('#linhaConsulta').html(linha);


    $('.modal-excluir').click(function(){
        $('.modal-excluir-dialog .nomePaciente').html($(this).attr('data-nome'));
        $('.modal-excluir-dialog .nomePaciente').attr('data-codigo',$(this).attr('data-codigo'));
        $('.modal-excluir-dialog .nomePaciente').attr('data-idAgenda',$(this).attr('data-idAgenda'));
        $('.modal-excluir-dialog').modal('show');
    });

}//fim do CarregaLinhaTabela

