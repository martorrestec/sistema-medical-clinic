$(document).ready(function() {

    function limpa_formulário_cep() {
        // Limpa valores do formulário de cep.
        $("#logradouro").val("");
        $("#bairro").val("");
        $("#cidade").val("");
        $("#estado").val("");
    }
    
    //Quando o campo cep perde o foco.
    $("#cep").blur(function() {

        //Nova variável "cep" somente com dígitos.
        var cep = $(this).val().replace(/\D/g, '');

        //Verifica se campo cep possui valor informado.
        if (cep != "") {

            //Expressão regular para validar o CEP.
            var validacep = /^[0-9]{8}$/;

            //Valida o formato do CEP.
            if(validacep.test(cep)) {

                //Preenche os campos com "..." enquanto consulta webservice.
                $("#logradouro, #bairro, #cidade, #estado").val("...");

                //Consulta o webservice viacep.com.br/
                $.getJSON("https://viacep.com.br/ws/"+ cep +"/json/?callback=?", function(dados) {

                    if (!("erro" in dados)) {
                        //Atualiza os campos com os valores da consulta.
                        $("#logradouro").val(dados.logradouro);
                        $("#bairro").val(dados.bairro);
                        $("#cidade").val(dados.localidade);
                        $("#estado").val(dados.uf);
                    } //end if.
                    else {
                        //CEP pesquisado não foi encontrado.
                        limpa_formulário_cep();
                        alert("CEP não encontrado. Preencha os dados do seu endereço.");
                        $("#logradouro, #bairro, #cidade, #estado").removeAttr("disabled");
                        $("#logradouro").trigger('focus');
                    }
                });
            } //end if.
            else {
                //cep é inválido.
                limpa_formulário_cep();
                alert("Formato de CEP inválido.");
            }
        } //end if.
        else {
            //cep sem valor, limpa formulário.
            limpa_formulário_cep();
            alert("Preencha o CEP.");
        }
    });

    CarregaPacientes();
    
    $('.pesquisar').on('click',function(){
        var linhaCarregando = $("<tr><td class='loading text-center' colspan='5'><img width='15' class='mr-2' src='../includes/image/ajax-loader.gif'/><span>Carregando...</span></td></tr>");
        var termo = $('.txtPesquisar').val();

        $.ajax({
            url : '../includes/server/restrito/restrito-pacientes.php',
            dataType : 'json',
            type : 'POST',
            data : {'modulo' : 'pesquisar', 'termo' : termo},
            beforeSend : function(){
                $('#linhaPaciente').html('');
            	$('#linhaPaciente').append(linhaCarregando);
            },
            complete : function(){
            	$(linhaCarregando).remove();
            },
            success : function(dados, textStatus){
                //console.log(dados);
                if(dados.length != 0) {
                    $('#linhaPaciente').html('');
                    CarregaLinhaTabela(dados);
                } else {
                    $('#linhaPaciente').html('<tr><td class="loading text-center" colspan="5">Pesquisa sem resultados.</td></tr>');
                }
                
            }
        });
    });

    //Chamada para os módulos de Modal
    $('.modal-add').click(function(){
        //console.log('clicou');
        $('.modal-pacientes .modal-title').html('Cadastrar Pacientes');
        $('.modal-pacientes .btn-action').html('Cadastrar');
        $('.modal-pacientes form').trigger("reset");
        
        $('.incluirPaciente').removeClass('d-none');
        $('.consultarPaciente, .alterarPaciente').addClass('d-none');
        $('.modal-pacientes').modal('show');
    });
    
    $('.incluirPaciente').on('click',function(){
        
        codigo = $('#codigo').val();
        nome = $('#nome').val();
        email = $('#email').val();
        cpf = $('#cpf').val();
        identidade = $('#identidade').val();
        dtNasc = ConverteDataMysql($('#dtNasc').val());
        estCivil = $('#estCivil').val();
        sexo = $('#sexo').val();
        phone = $('#phone').val();
        nomePai = $('#nomePai').val();
        nomeMae = $('#nomeMae').val();
        cep = $('#cep').val();
        logradouro = $('#logradouro').val();
        numero = $('#numero').val();
        complemento = $('#complemento').val();
        bairro = $('#bairro').val();
        cidade = $('#cidade').val();
        estado = $('#estado').val();        
        
        if( nome == '' || email == '' || cpf == '' || dtNasc == '' || phone == '') {
            alert('Verifique o preenchimento de todos os campos! (NOME, EMAIL, CPF, DATA NASCIMENTO, TELEFONE)')
            return false;
        }

        $.ajax({
            url : '../includes/server/restrito/restrito-pacientes.php',
            dataType : 'text',
            type : 'POST',
            data : {
                'modulo' : 'incluir', 'id': codigo, 'nome' : nome, 'email' : email, 'cpf' : cpf, 'identidade' : identidade, 'dtNasc' : dtNasc, 'estCivil' : estCivil, 'sexo' : sexo, 'telefone' : phone, 'nomePai' : nomePai, 'nomeMae' : nomeMae, 'cep' : cep, 'endereco' : logradouro, 'numero' : numero, 'comple' : complemento, 'bairro' : bairro, 'cidade' : cidade, 'estado' : estado },
            success : function(dados, textStatus){
                dados = JSON.parse(dados);
                alert(dados.nome +' cadastrado com sucesso!');
                $('.modal-pacientes').modal('hide');
                $('.modal-pacientes form').trigger("reset");
                CarregaPacientes();
            }
        });
    });

    $('.alterarPaciente').on('click', function(){
        //console.log('vamos alterar');

        codigo = $('#codigo').val();
        nome = $('#nome').val();
        email = $('#email').val();
        cpf = $('#cpf').val();
        identidade = $('#identidade').val();
        dtNasc = ConverteDataMysql($('#dtNasc').val());
        estCivil = $('#estCivil').val();
        sexo = $('#sexo').val();
        phone = $('#phone').val();
        nomePai = $('#nomePai').val();
        nomeMae = $('#nomeMae').val();
        cep = $('#cep').val();
        logradouro = $('#logradouro').val();
        numero = $('#numero').val();
        complemento = $('#complemento').val();
        bairro = $('#bairro').val();
        cidade = $('#cidade').val();
        estado = $('#estado').val();
        
        if( nome == '' || email == '' || cpf == '' || dtNasc == '' || phone == '') {
            alert('Verifique o preenchimento de todos os campos!')
            return false;
        }
        $.ajax({
            url : '../includes/server/restrito/restrito-pacientes.php',
            dataType : 'text',
            type : 'POST',
            data : {
                'modulo' : 'alterar', 'id': codigo, 'nome' : nome, 'email' : email, 'cpf' : cpf, 'identidade' : identidade, 'dtNasc' : dtNasc, 'estCivil' : estCivil, 'sexo' : sexo, 'telefone' : phone, 'nomePai' : nomePai, 'nomeMae' : nomeMae, 'cep' : cep, 'endereco' : logradouro, 'numero' : numero, 'comple' : complemento, 'bairro' : bairro, 'cidade' : cidade, 'estado' : estado },
            success : function(dados, textStatus){
                dados = JSON.parse(dados);
                //console.log(dados);
                alert(dados.nome +' alterado com sucesso!');
                $('.modal-pacientes').modal('hide');
                $('.modal-pacientes form').trigger("reset");
                CarregaPacientes();
            }
        });

    });

    $('.excluirPaciente').on('click', function(){

        codigo = $('.modal-excluir-dialog .nomePaciente').attr('data-codigo');
        nome = $('.modal-excluir-dialog .nomePaciente').html();

        $.ajax({
            url : '../includes/server/restrito/restrito-pacientes.php',
            dataType : 'text',
            type : 'POST',
            data : { 'modulo' : 'excluir', 'id': codigo, 'nome':nome },
            success : function(dados, textStatus){
                dados = JSON.parse(dados);
                //console.log(dados);
                alert(dados.nome +' excluído com sucesso!');
                $('.modal-excluir-dialog').modal('hide');
                $('#tr_' + codigo).remove();                
            }
        });
    });

});

function ConsultaPaciente(codigo, operacao) {

    //console.log(codigo);
    $.ajax({
        url : '../includes/server/restrito/restrito-pacientes.php',
        dataType : 'json',
        type : 'POST',
        data : { 'modulo' : 'especifica', 'operacao' : operacao, 'id': codigo },
        success : function(dados, textStatus){
            //dados = JSON.parse(dados);
            //console.log(dados);
            $('.modal-pacientes form').trigger("reset");
            $('#codigo').val(dados[0].id);
            $('#nome').val(dados[0].nome);
            $('#email').val(dados[0].email);
            $('#cpf').val(dados[0].cpf);
            $('#identidade').val(dados[0].identidade);
            $('#dtNasc').val(FormataDataMysql(dados[0].dataNasc));
            $('#estCivil').val(dados[0].estadocivil);
            $('#sexo').val(dados[0].sexo);
            $('#phone').val(dados[0].telefone);
            $('#nomePai').val(dados[0].nomePai);
            $('#nomeMae').val(dados[0].nomeMae);
            $('#cep').val(dados[0].cep);
            $('#logradouro').val(dados[0].endereco);
            $('#numero').val(dados[0].numero);
            $('#complemento').val(dados[0].complemento);
            $('#bairro').val(dados[0].bairro);
            $('#cidade').val(dados[0].cidade);
            $('#estado').val(dados[0].estado);
                                    
        }
    });
}//fim do document ready

function CarregaPacientes() {
    var linhaCarregando = $("<tr><td class='loading text-center' colspan='5'><img width='15' class='mr-2' src='../includes/image/ajax-loader.gif'/><span>Carregando...</span></td></tr>");
    
	$.ajax({

		url : '../includes/server/restrito/restrito-pacientes.php',
		dataType : 'json',
		type : 'POST',
        data : {'modulo' : 'listaPacientes'},
        beforeSend : function(){
            $('#linhaPaciente').html('');
            $('#linhaPaciente').append(linhaCarregando);
        },
        complete : function(){
            $(linhaCarregando).remove();
        },
		success : function(dados, textStatus){
            //console.log(dados);
            $('#linhaPaciente').html('');
            CarregaLinhaTabela(dados);
		}
	});

}//fim do CarregaExames()

function CarregaLinhaTabela(paciente) {

    var linha = '';

    for(var i=0; i<paciente.length; i++) {
        linha+='<tr id="tr_'+paciente[i].id+'"><td class="text-center">'+paciente[i].id+'</td><td>'+paciente[i].nome+'</td><td class="text-center">'+paciente[i].cpf+'</td><td class="text-center"><button data-codigo="'+paciente[i].id+'" class="border-0 bg-transparent modal-consultar" type="button" title="Consultar '+paciente[i].nome+'"><i class="fas fa-file-medical-alt text-blue"></i></button><button data-codigo="'+paciente[i].id+'" class="border-0 bg-transparent modal-alterar" type="button" title="Alterar '+paciente[i].nome+'"><i class="fas fa-file-signature text-green"></i></button><button onclick="window.location.href=\'marcar-consulta.php?modulo=especifica&id='+paciente[i].id+'\'" data-codigo="'+paciente[i].id+'" class="border-0 bg-transparent" type="button" data-toggle="modal" data-target=".consultar-paciente" title="Marcar Consulta de '+paciente[i].nome+'"><i class="fas fa-calendar-plus text-blue"></i></button><button data-codigo="'+paciente[i].id+'" class="border-0 bg-transparent historico" type="button"  title="Histórico de Consulta de '+paciente[i].nome+'"><i class="far fa-calendar-alt text-blue"></i></button><button data-codigo="'+paciente[i].id+'" data-nome="'+paciente[i].nome+'" class="border-0 bg-transparent modal-excluir" type="button" title="Excluir '+paciente[i].nome+'"><i class="fas fa-times text-red"></i></button></td>';
    }

    $('#linhaPaciente').html(linha);

    //adiciono os eventos no demais botões
    $('.modal-consultar').click(function(){
        $('.modal-pacientes .modal-title').html('Consultar Paciente');
        $('.modal-pacientes .btn-action').html('Consultar');
        $('.modal-pacientes [name*="txt"]').attr('disabled',true);
        ConsultaPaciente($(this).attr('data-codigo'), 'consultar');
        $('.consultarPaciente, .incluirPaciente, .alterarPaciente').addClass('d-none');
        $('.modal-pacientes').modal('show');
    });
    
    $('.modal-alterar').click(function(){
        $('.modal-pacientes .modal-title').html('Alterar Paciente');
        $('.modal-pacientes .btn-action').html('Alterar');
        $('.modal-pacientes [name*="txt"]').attr('disabled',false);
        $('#codigo, #dtCad').attr('disabled',true);
        ConsultaPaciente($(this).attr('data-codigo'), 'alterar');
        $('.alterarPaciente').removeClass('d-none');
        $('.consultarPaciente, .incluirPaciente').addClass('d-none');
        $('.modal-pacientes').modal('show');
    });

    $('.modal-excluir').click(function(){
        $('.modal-excluir-dialog .nomePaciente').html($(this).attr('data-nome'));
        $('.modal-excluir-dialog .nomePaciente').attr('data-codigo',$(this).attr('data-codigo'));
        $('.modal-excluir-dialog').modal('show');
    });
    $('.historico').click(function(){
        ConsultaAgendaPaciente($(this).attr('data-codigo'));
        $('.modal-historico').modal('show');
    });

}//fim do CarregaLinhaTabela

function ConsultaAgendaPaciente(codigo) {

    $('#linhaConsulta').html(''); 
    $.ajax({
        url : '../includes/server/restrito/restrito-agenda.php',
        dataType : 'json',
        type : 'POST',
        data : { 'modulo' : 'buscaAgendaPaciente', 'operacao' : 'consulta', 'id': codigo },
        success : function(dados, textStatus){
            //dados = JSON.parse(dados);
            console.log(dados);            
            var linha = '';
            for(var i=0; i<dados.length; i++) {
                linha+='<tr id="tr_ag_'+dados[i].id_agenda+'"><td class="text-center">'+dados[i].id_agenda+'</td><td>'+dados[i].nome_medico+'</td><td class="text-center">'+FormataDataMysql(dados[i].data.split(' ')[0])+'</td><td class="text-center">'+dados[i].data.split(' ')[1].substr(0,5)+'</td><td>'+dados[i].nome_paciente+'</td></tr>';
            }
            $('#linhaConsulta').html(linha);   
        }
    });

}//fim do ConsultaAgendaPaciente

function ConverteDataMysql(data) {
    dataNova = data.split('/');
    dataMysql = dataNova[2]+'/'+dataNova[1]+'/'+dataNova[0];
    return (dataMysql);s
} //fim da ConverteDataMysql
function FormataDataMysql(data) {
    dataNova = data.split('-');
    dataMysql = dataNova[2]+'/'+dataNova[1]+'/'+dataNova[0];
    return (dataMysql);s
} //fim da ConverteDataMysql