$(document).ready(function(){
    //Controlo a exibição dos campos de horário do medicos
    CarregaExames();
    
    $('.pesquisar').on('click',function(){
        var linhaCarregando = $("<tr><td class='loading text-center' colspan='5'><img width='15' class='mr-2' src='../includes/image/ajax-loader.gif'/><span>Carregando...</span></td></tr>");
        var termo = $('.txtPesquisar').val();

        $.ajax({
            url : '../includes/server/restrito/restrito-exames.php',
            dataType : 'json',
            type : 'POST',
            data : {'modulo' : 'pesquisar', 'termo' : termo},
            beforeSend : function(){
                $('#linhaExame').html('');
            	$('#linhaExame').append(linhaCarregando);
            },
            complete : function(){
            	$(linhaCarregando).remove();
            },
            success : function(dados, textStatus){
                console.log(dados);
                if(dados.length != 0) {
                    $('#linhaExame').html('');
                    CarregaLinhaTabela(dados);
                } else {
                    $('#linhaExame').html('<tr><td class="loading text-center" colspan="5">Pesquisa sem resultados.</td></tr>');
                }
                
            }
        });
    });

    //Chamada para os módulos de Modal
    $('.modal-add').click(function(){
        //console.log('clicou');
        $('.modal-exames .modal-title').html('Cadastrar Exames');
        $('.modal-exames .btn-action').html('Cadastrar');
        $('.modal-exames form').trigger("reset");
        
        $('.incluirExame').removeClass('d-none');
        $('.consultarExame, .alterarExame').addClass('d-none');
        $('.modal-exames').modal('show');
    });
    
    $('.incluirExame').on('click',function(){
        
        codigo = $('#codigo').val();
        nome = $('#nomeExame').val();
        sigla = $('#siglaExame').val();
        
        
        if( nome == '' || sigla == '') {
            alert('Verifique o preenchimento de todos os campos!')
            return false;
        }

        $.ajax({
            url : '../includes/server/restrito/restrito-exames.php',
            dataType : 'text',
            type : 'POST',
            data : {
                'modulo' : 'incluir', 'id': codigo, 'nome' : nome, 'sigla' : sigla },
            success : function(dados, textStatus){
                dados = JSON.parse(dados);
                alert(dados.nome +' cadastrado com sucesso!');
                $('.modal-exames').modal('hide');
                $('.modal-exames form').trigger("reset");
                CarregaExames();
            }
        });
    });

    $('.alterarExame').on('click', function(){
        //console.log('vamos alterar');

        codigo = $('#codigo').val();
        nome = $('#nomeExame').val();
        sigla = $('#siglaExame').val();
        
        if( nome == '' || sigla == '' ) {
            alert('Verifique o preenchimento de todos os campos!')
            return false;
        }
        $.ajax({
            url : '../includes/server/restrito/restrito-exames.php',
            dataType : 'text',
            type : 'POST',
            data : {
                'modulo' : 'alterar', 'id': codigo, 'nome' : nome, 'sigla' : sigla, },
            success : function(dados, textStatus){
                dados = JSON.parse(dados);
                //console.log(dados);
                alert(dados.nome +' alterado com sucesso!');
                $('.modal-exames').modal('hide');
                $('.modal-exames form').trigger("reset");
                CarregaExames();
            }
        });

    });

    $('.excluirExame').on('click', function(){

        codigo = $('.modal-excluir-dialog .nomeExame').attr('data-codigo');
        nome = $('.modal-excluir-dialog .nomeExame').html();

        $.ajax({
            url : '../includes/server/restrito/restrito-exames.php',
            dataType : 'text',
            type : 'POST',
            data : { 'modulo' : 'excluir', 'id': codigo, 'nome':nome },
            success : function(dados, textStatus){
                dados = JSON.parse(dados);
                //console.log(dados);
                alert(dados.nome +' excluído com sucesso!');
                $('.modal-excluir-dialog').modal('hide');
                $('#tr_' + codigo).remove();                
            }
        });
    });
});    

function ConsultaExame(codigo, operacao) {

    //console.log(codigo);
    $.ajax({
        url : '../includes/server/restrito/restrito-exames.php',
        dataType : 'json',
        type : 'POST',
        data : { 'modulo' : 'especifica', 'operacao' : operacao, 'id': codigo },
        success : function(dados, textStatus){
            //dados = JSON.parse(dados);
            console.log(dados);
            $('.modal-exames form').trigger("reset");
            $('#codigo').val(dados[0].id);
            $('#nomeExame').val(dados[0].nome);
            $('#siglaExame').val(dados[0].sigla);                             
        }
    });
}//fim do document ready

function CarregaExames() {
    var linhaCarregando = $("<tr><td class='loading text-center' colspan='5'><img width='15' class='mr-2' src='../includes/image/ajax-loader.gif'/><span>Carregando...</span></td></tr>");
    
	$.ajax({

		url : '../includes/server/restrito/restrito-exames.php',
		dataType : 'json',
		type : 'POST',
        data : {'modulo' : 'listaExames'},
        beforeSend : function(){
            $('#linhaExame').html('');
            $('#linhaExame').append(linhaCarregando);
        },
        complete : function(){
            $(linhaCarregando).remove();
        },
		success : function(dados, textStatus){
            console.log(dados);
            $('#linhaExame').html('');
            CarregaLinhaTabela(dados);
		}
	});

}//fim do CarregaExames()

function CarregaLinhaTabela(exame) {

    var linha = '';

    for(var i=0; i<exame.length; i++) {
        linha+='<tr id="tr_'+exame[i].id+'"><td class="text-center">'+exame[i].id+'</td><td>'+exame[i].nome+'</td><td>'+exame[i].sigla+'</td><td class="text-center"><button data-codigo="'+exame[i].id+'" class="border-0 bg-transparent modal-consultar" type="button" title="Consultar '+exame[i].nome+'"><i class="fas fa-file-medical-alt text-blue"></i></button><button data-codigo="'+exame[i].id+'" class="border-0 bg-transparent modal-alterar" type="button" title="Alterar '+exame[i].nome+'"><i class="fas fa-file-signature text-green"></i></button><button data-codigo="'+exame[i].id+'" data-nome="'+exame[i].nome+'" class="border-0 bg-transparent modal-excluir" type="button" title="Excluir '+exame[i].nome+'"><i class="fas fa-times text-red"></i></button></td>';
    }

    $('#linhaExame').html(linha);

    //adiciono os eventos no demais botões
    $('.modal-consultar').click(function(){
        $('.modal-exames .modal-title').html('Consultar Exame');
        $('.modal-exames .btn-action').html('Consultar');
        $('.modal-exames [name*="txt"]').attr('disabled',true);
        ConsultaExame($(this).attr('data-codigo'), 'consultar');
        $('.consultarExame, .incluirExame, .alterarExame').addClass('d-none');
        $('.modal-exames').modal('show');
    });
    
    $('.modal-alterar').click(function(){
        $('.modal-exames .modal-title').html('Alterar Exame');
        $('.modal-exames .btn-action').html('Alterar');
        $('.modal-exames [name*="txt"]').attr('disabled',false);
        $('#codigo, #dtCad').attr('disabled',true);
        ConsultaExame($(this).attr('data-codigo'), 'alterar');
        $('.alterarExame').removeClass('d-none');
        $('.consultarExame, .incluirExame').addClass('d-none');
        $('.modal-exames').modal('show');
    });

    $('.modal-excluir').click(function(){
        $('.modal-excluir-dialog .nomeExame').html($(this).attr('data-nome'));
        $('.modal-excluir-dialog .nomeExame').attr('data-codigo',$(this).attr('data-codigo'));
        $('.modal-excluir-dialog').modal('show');
    });

    

}//fim do CarregaLinhaTabela

