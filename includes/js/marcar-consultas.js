$(document).ready(function() {

    //pego valores de GET da URL e armazeno num Objeto
    var query = location.search.slice(1);
    var partes = query.split('&');
    var data = {};
    partes.forEach(function (parte) {
        var chaveValor = parte.split('=');
        var chave = chaveValor[0];
        var valor = chaveValor[1];
        data[chave] = valor;
    });

    CarregaDadosPaciente(data);
    CarregaMedicos();

    $('#medico').on('change', function(){
        $('.inseridos').remove();
        var idMedico = $(this).val();
        if(idMedico != '') {
            $.ajax({
                url : '../includes/server/restrito/restrito-consultas.php',
                dataType : 'json',
                type : 'POST',
                data : {'modulo' : 'listaAgenda', 'idMedico' : idMedico },
                success : function(dados, textStatus){
                    //console.log(dados);
                    for(var i=0; i<=dados.length; i++) {
                        $('#dataDisp').removeAttr('disabled');                        
                        $('#dataDisp').append('<option class="inseridos" value="'+dados[i].id_agenda+'">'+dados[i].data+'</option>');
                    }            
                }
            });
        }
    });
    $('.agendar').on('click', function(e){
        e.preventDefault();
        nome = $('#nome').val();
        medico = $('#medico option:selected').text();
        horario = $('#dataDisp option:selected').text();
        if(nome != '' && medico != '' && horario != '') {
            $('.nomePaciente').html('<strong>Paciente: </strong>'+nome);
            $('.nomeMedico').html('<strong>Médico:</strong> Dr. '+medico);
            $('.dataAtend').html('<strong>Data: </strong>'+horario.split(' ')[0]);
            $('.horaAtend').html('<strong>Horario: </strong>'+horario.split(' ')[1]);
            $('.painelConsulta').removeClass('d-none');
        }
    });//fim do click .agendar
    $('.marcarConsulta').on('click',function(){
        id_agenda = $('#dataDisp option:selected').val();
        nome_paciente = $('#nome').val();
        id_paciente = $('#codigo').val();
        $.ajax({
            url : '../includes/server/restrito/restrito-consultas.php',
            dataType : 'json',
            type : 'POST',
            data : {'modulo' : 'incluir', 'id_agenda' : id_agenda, 'nome_paciente': nome_paciente, 'id_paciente': id_paciente },
            success : function(dados, textStatus){
                //console.log(dados);
                alert('Consulta de '+dados.nome+' marcada com sucesso!');
                window.location.href = 'consultas.php';          
            }
        });

    });

});

function CarregaMedicos() {

    $.ajax({
		url : '../includes/server/restrito/restrito-consultas.php',
		dataType : 'json',
		type : 'POST',
        data : {'modulo' : 'listaMedica' },
		success : function(dados, textStatus){
            //console.log(dados);
            for(i=0; i<=dados.length; i++) {
                $('#medico').append('<option value="'+dados[i].id+'">'+dados[i].nome+'</option>')
            }            
		}
	});

}//fim do CarregaMedicos

function CarregaDadosPaciente(data) {
    
	$.ajax({
		url : '../includes/server/restrito/restrito-consultas.php',
		dataType : 'json',
		type : 'POST',
        data : {'modulo' : data.modulo, 'id' : data.id},
		success : function(dados, textStatus){
            //console.log(dados);
            $('#codigo').val(dados[0].id);
            $('#nome').val(dados[0].nome);
		}
	});

}//fim do CarregaExames()



