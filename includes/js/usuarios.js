$(document).ready(function(){
    //Controlo a exibição dos campos de horário do medicos
    CarregaUsuarios();

    $('.pesquisar').on('click',function(){
        var linhaCarregando = $("<tr><td class='loading text-center' colspan='5'><img width='15' class='mr-2' src='../includes/image/ajax-loader.gif'/><span>Carregando...</span></td></tr>");
        var termo = $('.txtPesquisar').val();

        $.ajax({
            url : '../includes/server/restrito/restrito-usuarios.php',
            dataType : 'json',
            type : 'POST',
            data : {'modulo' : 'pesquisar', 'termo' : termo},
            beforeSend : function(){
                $('#linhasUsuarios').html('');
            	$('#linhasUsuarios').append(linhaCarregando);
            },
            complete : function(){
            	$(linhaCarregando).remove();
            },
            success : function(dados, textStatus){
                console.log(dados);
                if(dados.length != 0) {
                    $('#linhasUsuarios').html('');
                    CarregaLinhaTabela(dados);
                } else {
                    $('#linhasUsuarios').html('<tr><td class="loading text-center" colspan="5">Pesquisa sem resultados.</td></tr>');
                }
                
            }
        });
    });

    $('#perfil').on('change',function(){
        if($(this).val() != 'ME'){
            $('.souMedico').fadeOut();
            $('[name="txtDiaTrabalho"], [name="txtTurnoTrabalho"]').prop('checked', false);
        } else {
            $('.souMedico').fadeIn();
        }
    });
    //Chamada para os módulos de Modal
    $('.modal-add').click(function(){
        $('.modal-usuario .modal-title').html('Cadastrar Usuário');
        $('.modal-usuario .btn-action').html('Cadastrar');
        $('.modal-usuario form').trigger("reset");
        $('.modal-usuario [name*="txt"]').attr('disabled',false);
        $('#codigo, #dtCad').attr('disabled',true);
        $('.incluirUsuario').removeClass('d-none');
        $('.consultarUsuario, .alterarUsuario').addClass('d-none');
        $('.modal-usuario').modal('show');
    });
    
    $('.incluirUsuario').on('click',function(){
        
        codigo = $('#codigo').val();
        nome = $('#nome').val();
        email = $('#email').val();
        cargo = $('#cargo').val();
        login = $('#login').val();
        senha = $('#senha').val();
        dtCad = $('#dtCad').val();
        perfil = $('#perfil').val();
       
        if(nome == '' || email == '' || cargo == '' || login == '' || senha == '' || perfil == '') {
            alert('Verifique o preenchimento dos campos (NOME, EMAIL, CARGO, LOGIN, SENHA, PERFIL)!')
            return false;
        }

        diasTrab = $('input[name="txtDiaTrabalho"]:checked').toArray().map(function(check) { 
            return $(check).val(); 
        }); 
        turnoTrab = $('input[name="txtTurnoTrabalho"]:checked').toArray().map(function(check) { 
            return $(check).val(); 
        }); 

        $.ajax({
            url : '../includes/server/restrito/restrito-usuarios.php',
            dataType : 'text',
            type : 'POST',
            data : {
                'modulo' : 'incluir', 'nome': nome, 'email' : email, 'cargo' : cargo, 'login' : login, 'senha' : senha, 'dataCad' : dtCad, 'perfil' : perfil, 'diasTrab' : diasTrab.join(","), 'turnoTrab' : turnoTrab.join(",") },
            success : function(dados, textStatus){
                dados = JSON.parse(dados);
                alert(dados.nome +' cadastrado com sucesso!');
                $('.modal-usuario').modal('hide');
                $('.modal-usuario form').trigger("reset");
                CarregaUsuarios();
            }
        });
    });

    $('.alterarUsuario').on('click', function(){
        //console.log('vamos alterar');

        codigo = $('#codigo').val();
        nome = $('#nome').val();
        email = $('#email').val();
        cargo = $('#cargo').val();
        login = $('#login').val();
        senha = $('#senha').val();
        dtCad = $('#dtCad').val();
        perfil = $('#perfil').val();
       
        if(nome == '' || email == '' || cargo == '' || login == '' || senha == '' || perfil == '') {
            alert('Verifique o preenchimento dos campos (NOME, EMAIL, CARGO, LOGIN, SENHA, PERFIL)!')
            return false;
        }

        diasTrab = $('input[name="txtDiaTrabalho"]:checked').toArray().map(function(check) { 
            return $(check).val(); 
        }); 
        turnoTrab = $('input[name="txtTurnoTrabalho"]:checked').toArray().map(function(check) { 
            return $(check).val(); 
        }); 

        $.ajax({
            url : '../includes/server/restrito/restrito-usuarios.php',
            dataType : 'text',
            type : 'POST',
            data : {
                'modulo' : 'alterar', 'id': codigo, 'nome': nome, 'email' : email, 'cargo' : cargo, 'login' : login, 'senha' : senha, 'dataCad' : dtCad, 'perfil' : perfil, 'senha': senha ,'diasTrab' : diasTrab.join(","), 'turnoTrab' : turnoTrab.join(",") },
            success : function(dados, textStatus){
                dados = JSON.parse(dados);
                //console.log(dados);
                alert(dados.nome +' alterado com sucesso!');
                $('.modal-usuario').modal('hide');
                $('.modal-usuario form').trigger("reset");
                CarregaUsuarios();
            }
        });

    });

    $('.excluirUsuario').on('click', function(){

        codigo = $('.modal-excluir-dialog .nomeUsuario').attr('data-codigo');
        nome = $('.modal-excluir-dialog .nomeUsuario').html();

        $.ajax({
            url : '../includes/server/restrito/restrito-usuarios.php',
            dataType : 'text',
            type : 'POST',
            data : { 'modulo' : 'excluir', 'id': codigo, 'nome':nome },
            success : function(dados, textStatus){
                dados = JSON.parse(dados);
                //console.log(dados);
                alert(dados.nome +' excluído com sucesso!');
                $('.modal-excluir-dialog').modal('hide');
                $('#tr_' + codigo).remove();                
            }
        });
    });
});    

function ConsultaUsuario(codigo, operacao) {

    //console.log(codigo);
    $.ajax({
        url : '../includes/server/restrito/restrito-usuarios.php',
        dataType : 'json',
        type : 'POST',
        data : { 'modulo' : 'especifica', 'operacao' : operacao, 'codigo': codigo },
        success : function(dados, textStatus){
            //dados = JSON.parse(dados);
            //console.log(dados);
            $('.modal-usuario form').trigger("reset");
            $('#codigo').val(dados[0].id);
            $('#nome').val(dados[0].nome);
            $('#email').val(dados[0].email);
            $('#cargo').val(dados[0].cargo);
            $('#login').val(dados[0].login);
            $('#senha').val(dados[0].senha);
            $('#dtCad').val(dados[0].dataCad);
            $('#perfil').val(dados[0].perfil);
            
            $('[name="txtTurnoTrabalho"]').each(function(){
                turnoTrab = dados[0].turnoTrab.split(',');
                for(i=0;i<turnoTrab.length;i++) {
                    if($(this).val() == turnoTrab[i]){                        
                        $(this).prop('checked', true);
                    }
                }
            });
            $('[name="txtDiaTrabalho"]').each(function(){
                turnoTrab = dados[0].diasTrab.split(',');
                for(i=0;i<turnoTrab.length;i++) {
                    if($(this).val() == turnoTrab[i]){                        
                        $(this).prop('checked', true);
                    }
                }
            });
            
        }
    });

}//fim do CarregaUsuarios

function CarregaUsuarios() {
    var linhaCarregando = $("<tr><td class='loading text-center' colspan='5'><img width='15' class='mr-2' src='../includes/image/ajax-loader.gif'/><span>Carregando...</span></td></tr>");

	$.ajax({

		url : '../includes/server/restrito/restrito-usuarios.php',
		dataType : 'json',
		type : 'POST',
		data : {'modulo' : 'listaUsuarios'},
		beforeSend : function(){
            $('#linhasUsuarios').html('');
            $('#linhasUsuarios').append(linhaCarregando);
        },
        complete : function(){
            $(linhaCarregando).remove();
        },
		success : function(dados, textStatus){
            //console.log(dados);
            $('#linhasUsuarios').html('');
            CarregaLinhaTabela(dados);
		}
	});

}//fim do CarregaUsuarios()

function CarregaLinhaTabela(usuario) {

    var linha = '';

    for(var i=0; i<usuario.length; i++) {
        linha+='<tr id="tr_'+usuario[i].id+'"><td class="text-center">'+usuario[i].id+'</td><td>'+usuario[i].nome+'</td><td class="text-center">'+usuario[i].login+'</td><td class="text-center">'+AjustaPerfil(usuario[i].perfil)+'</td><td class="text-center"><button data-codigo="'+usuario[i].id+'" class="border-0 bg-transparent modal-consultar" modulo="consultar" type="button" title="Consultar '+usuario[i].nome+'"><i class="fas fa-address-card text-blue"></i></button><button data-codigo="'+usuario[i].id+'" class="border-0 bg-transparent modal-alterar" modulo="alterar" type="button" title="Alterar '+usuario[i].nome+'"><i class="fas fa-user-edit text-green"></i></button><button data-codigo="'+usuario[i].id+'" data-nome="'+usuario[i].nome+'" class="border-0 bg-transparent modal-excluir" modulo="excluir" type="button" title="Excluir '+usuario[i].nome+'"><i class="fas fa-user-times text-red"></i></button><button data-codigo="'+usuario[i].id+'" class="border-0 bg-transparent modal-log" type="button" data-toggle="modal" data-target=".log-usuario" title="Log de Acesso '+usuario[i].nome+'"><i class="fas fa-file-alt text-gray333"></i></button></td></tr>';
    }

    $('#linhasUsuarios').html(linha);

    //adiciono os eventos no demais botões
    $('.modal-consultar').click(function(){
        $('.modal-usuario .modal-title').html('Consultar Usuário');
        $('.modal-usuario .btn-action').html('Consultar');
        $('.modal-usuario [name*="txt"]').attr('disabled',true);
        ConsultaUsuario($(this).attr('data-codigo'), 'consultar');
        $('.consultarUsuario, .incluirUsuario, .alterarUsuario').addClass('d-none');
        $('.modal-usuario').modal('show');
    });
    
    $('.modal-alterar').click(function(){
        $('.modal-usuario .modal-title').html('Alterar Usuário');
        $('.modal-usuario .btn-action').html('Alterar');
        $('.modal-usuario [name*="txt"]').attr('disabled',false);
        $('#codigo, #dtCad').attr('disabled',true);
        ConsultaUsuario($(this).attr('data-codigo'), 'alterar');
        $('.alterarUsuario').removeClass('d-none');
        $('.consultarUsuario, .incluirUsuario').addClass('d-none');
        $('.modal-usuario').modal('show');
    });

    $('.modal-excluir').click(function(){
        $('.modal-excluir-dialog .nomeUsuario').html($(this).attr('data-nome'));
        $('.modal-excluir-dialog .nomeUsuario').attr('data-codigo',$(this).attr('data-codigo'));
        $('.modal-excluir-dialog').modal('show');
    });

    $('.modal-log').click(function(){
        //console.log($(this).attr("data-codigo"));
        $('.tabela-logs').html('');
        $('.tabela-logs').load('logs/log_acesso_'+ $(this).attr("data-codigo")+'.html', function( response, status, xhr ){
            if(status == 'error') {
                $(this).html('<tr><td colspan="5" class="text-center">Usuário sem dados de log.</td></tr>');
            }
        });
        $('.log-usuario').modal('show');
    });

}//fim do CarregaLinhaTabela

function AjustaPerfil($perfil) {
    switch($perfil) {
        case 'AD' :
            $descPerfil = 'Administrador';
            break;
        case 'ME' :
            $descPerfil = 'Médico';
            break;
        case 'GE' :
            $descPerfil = 'Gerente';
            break;
        case 'AT' :
            $descPerfil = 'Atendente';
            break;
        default :
            $descPerfil = 'Sem Perfil';
            break;
    }
    return $descPerfil;
}//fim do Perfil()