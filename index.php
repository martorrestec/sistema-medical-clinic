<!DOCTYPE html>
<html lang="pt-br">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="icon" type="image/png" sizes="16x16" href="includes/image/favicon.png">
    <link rel="stylesheet" href="includes/css/bootstrap.min.css">
    <link rel="stylesheet" href="includes/css/all.min.css">
    <link rel="stylesheet" href="includes/css/style.css">
    <title>ENTRAR NO SISTEMA MEDICAL CLINIC</title>
  </head>
  <body class="home-signin">
    <div class="container">
      <div class="row">
        <div class="col-sm-9 col-md-7 col-lg-5 mx-auto">
          <div class="card my-5 card-signin">
            <div class="card-body ">
              <h5 class="card-title text-center my-5">
                  <img src="includes/image/logo_sistema_medical_clinic.png" alt="Logo Medical Clinic">
              </h5>
              <form class="form-signin" action="login.php" method="post">
                <div class="form-label-group mb-4">
                  <input type="text" id="inputEmail" name="txtLogin" class="form-control" placeholder="Usuário ou Email" required autofocus>
                </div>

                <div class="form-label-group mb-1">
                <input type="password" id="inputPassword" name="txtPassword" class="form-control" placeholder="Senha" required>
                </div>

                <div class="custom-control custom-checkbox mb-3">
                  <input type="hidden" id="modulo" name="txtModulo" class="form-control" value="logar">
                  <!-- <input type="checkbox" class="custom-control-input" id="customCheck1">
                  <label class="custom-control-label" for="customCheck1">Esqueci a senha</label> -->
                </div>
                <button class="btn btn-lg btn-success btn-block text-uppercase" type="submit">Entrar</button>
                <hr class="my-4"> 
                <p class="text-center text-14 <?= isset($_GET['msg']) ? 'alert alert-danger" role="alert"' : '';?>""><?= urldecode(@$_GET['msg']); ?></p>               
              </form>
            </div>
          </div>
        </div>
      </div>
    </div> 
    <!-- Dependecias -->
    <script src="includes/js/jquery-3.3.1.slim.min.js" ></script>
    <script src="includes/js/popper.min.js"></script>
    <script src="includes/js/all.min.js"></script>
    <script src="includes/js/bootstrap.min.js"></script>
    <script src="includes/js/scripts.js"></script>
  </body>
</html>