<?php
session_start();

// grava log de acesso
$data = date("d/m/Y H:i:s");
$linha = '<tr><td class="text-center">SAI</td><td class="text-center">'.$_SESSION['id'].'</td><td>'.$_SESSION['nome'].'</td><td class="text-center">'.$_SESSION['login'].'</td><td class="text-center">'.$data.'</td></tr>';

// grava a linha no arquivo.
$arq = fopen('admin/logs/log_acesso_'.$_SESSION['id'].'.html', 'a+');
fwrite($arq, $linha);	
fclose($arq);

session_destroy();
require_once('includes/server/urls.php');

header("Location: ".$GLOBALS['url_base']."/index.php?msg=Sessão%20Finalizada%20com%20Sucesso");

?>