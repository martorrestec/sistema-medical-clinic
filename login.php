<?php

session_set_cookie_params(600);
session_start();

function RecuperaDados() {

	// Verifica se o formulário foi submetido
	if($_SERVER['REQUEST_METHOD'] == 'POST'){    	
   		$post = $_POST;
   		// Verifica se todos campos vieram pelo formulário e se estão preenchidos
    	if(isset($post['txtModulo']) && isset($post['txtLogin']) && isset($post['txtPassword'])){
    		foreach ($post as $key => $value) {
				$_SESSION[$key] = filter_var($value);
			}//fim do foreach
		}//fim do if
	}//fim do if

}//fim do RecuperaDados

function ValidaLogin() {

	//print_r($_SESSION);

	require_once('includes/server/config.php');

	// Prepara uma sentença para ser executada
    $statement = $pdo->prepare("SELECT * FROM usuario WHERE login = :username AND senha = :password");
    
    // Adiciona os dados acima para serem executados na sentença
    $statement->bindParam(':username', $_SESSION['txtLogin']);
    $statement->bindParam(':password', md5($_SESSION['txtPassword']));
    
    // Executa a sentença já com os valores

    if($statement->execute()){
        // Definimos a mensagem de sucesso
    	if($statement->rowCount() == 1) {
    		$resultado = $statement->fetch(PDO::FETCH_ASSOC);    		
    		foreach($resultado as $key => $value){
    			$_SESSION[$key] = $value;
    		}
    		$_SESSION['descPerfil'] = AjustaPerfil($_SESSION['perfil']);
            $_SESSION['auth'] = true;
            
            // grava log de acesso
            $data = date("d/m/Y H:i:s");
            $linha = '<tr><td class="text-center">ENT</td><td class="text-center">'.$_SESSION['id'].'</td><td>'.$_SESSION['nome'].'</td><td class="text-center">'.$_SESSION['login'].'</td><td class="text-center">'.$data.'</td></tr>';

            // grava a linha no arquivo.
            $arq = fopen('admin/logs/log_acesso_'.$_SESSION['id'].'.html', 'a+');
            fwrite($arq, $linha);	
            fclose($arq);
    

    		header('Location: admin/home.php');
    	} else {
        	session_destroy();        	
        	header("Location: index.php?msg=Login%20ou%20Senhas%20Inválidos");
    	}   	

    } else {
        // Definimos a mensagem de erro
        $msg = 'Sistema%20Fora%20do%20Ar!%20Tente%20mais%20tarde...';
        session_destroy();
        header("Location: index.php?msg=$msg");
    } 


}//fim do ValidaLogin

function AjustaPerfil($perfil) {
    switch($perfil) {
        case 'AD' :
            $descPerfil = 'Administrador';
            break;
        case 'ME' :
            $descPerfil = 'Médico';
            break;
        case 'GE' :
            $descPerfil = 'Gerente';
            break;
        case 'AT' :
            $descPerfil = 'Atendente';
            break;
        default :
            $descPerfil = 'Sem Perfil';
            break;
    }
    return $descPerfil;
}//fim do Perfil()

RecuperaDados();

switch (@$_SESSION['txtModulo']) {
	case 'logar':
		ValidaLogin();
		break;
	
	default:
		break;
}



?>